package org.ocaml.lang.reason.psi

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiManager
import com.intellij.psi.search.FileTypeIndex
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.util.PsiTreeUtil
import org.ocaml.lang.reason.ReasonFileType
import org.ocaml.lang.shared.psi.ORElement
import org.ocaml.lang.shared.psi.ext.ORNamedElement


object ReasonPsiUtil {
    fun findSimpleTypeUsesByName(project: Project, key: String): List<ReasonPolymorphicTypeUse> =
            findGenericDefinitions(project, ReasonPolymorphicTypeUse::class.java)
                    .filter { it.identifier.text == key }

    fun findModuleUsesByName(project: Project, name: String): List<ReasonModuleName> =
            findGenericDefinitions(project, ReasonModuleName::class.java)
                    .filter { it.capitalidentifier.text == name }


    //    TODO: Make these nicer to use / more abastract
    fun findTypeDefinitionsByName(project: Project, key: String): List<ReasonTypeDefinition> =
            findGenericDefinitionsByName(project, key, ReasonTypeDefinition::class.java)

    fun findTypeDefinitions(project: Project): List<ReasonTypeDefinition> =
            findGenericDefinitions(project, ReasonTypeDefinition::class.java)

    fun findModuleDefinitionsByName(project: Project, key: String): List<ReasonModuleDefinitionStmt> =
            findGenericDefinitionsByName(project, key, ReasonModuleDefinitionStmt::class.java)

    private fun <T : ORNamedElement> findGenericDefinitionsByName(project: Project, key: String, vararg classes: Class<T>) =
            findGenericDefinitions(project, *classes)
                    .filter {
                        key == it.name.toString()
                    }

    private fun <T : ORElement> findGenericDefinitions(project: Project, vararg classes: Class<T>): List<T> {
        val psiManager = PsiManager.getInstance(project)

        return FileTypeIndex.getFiles(ReasonFileType, GlobalSearchScope.allScope(project))
                .mapNotNull {
                    psiManager.findFile(it) as ReasonPsiFile?
                }
                .map {
                    PsiTreeUtil.findChildrenOfAnyType(it, *classes)
                }
                .flatten()
    }
}
