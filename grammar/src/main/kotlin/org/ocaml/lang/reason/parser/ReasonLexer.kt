package org.ocaml.lang.reason.parser

import com.intellij.lexer.FlexAdapter
import org.ocaml.lang.shared.lexer._ORLexer
import org.ocaml.lang.shared.psi.ReasonTypeBridge

object ReasonLexer : FlexAdapter(_ORLexer(ReasonTypeBridge))
