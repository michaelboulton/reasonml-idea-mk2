package org.ocaml.lang.reason

import com.intellij.openapi.fileTypes.ExtensionFileNameMatcher
import com.intellij.openapi.fileTypes.FileTypeConsumer
import com.intellij.openapi.fileTypes.FileTypeFactory

object ReasonFileTypeFactory : FileTypeFactory() {
    override fun createFileTypes(consumer: FileTypeConsumer) {
        consumer.consume(
                ReasonFileType,
                ExtensionFileNameMatcher(ReasonFileType.defaultExtension)
        )
    }
}
