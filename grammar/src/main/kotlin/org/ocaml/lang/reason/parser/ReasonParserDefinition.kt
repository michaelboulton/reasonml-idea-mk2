package org.ocaml.lang.reason.parser

import org.ocaml.lang.reason.psi.ReasonPsiFile
import org.ocaml.lang.reason.psi.ReasonTypes
import org.ocaml.lang.shared.parser.BaseParserDefinition

class ReasonParserDefinition : BaseParserDefinition(
        ReasonLexer,
        ReasonParser(),
        { ReasonPsiFile(it) },
        { ReasonTypes.Factory.createElement(it) }
)
