package org.ocaml.lang.reason.psi

import com.intellij.psi.tree.IElementType
import com.intellij.psi.tree.TokenSet
import org.ocaml.lang.shared.psi.ORBlockLevelStmt
import org.ocaml.lang.shared.psi.ORInterfaceLevelStmt
import org.ocaml.lang.shared.psi.ORModuleLevelStmt


fun tokenSetOf(vararg tokens: IElementType) = TokenSet.create(*tokens)


/**
 * Standard keywords
 */
val REASON_KEYWORDS = tokenSetOf(
        ReasonTypes.AND,
        ReasonTypes.ARROW,
        ReasonTypes.AS,
        ReasonTypes.CONSTRAINT,
        ReasonTypes.DOWNTO,
        ReasonTypes.ELSE,
        ReasonTypes.EXCEPTION,
        ReasonTypes.EXTERNAL,
        ReasonTypes.FAILWITH,
        ReasonTypes.FOR,
        ReasonTypes.FUN,
        ReasonTypes.IF,
        ReasonTypes.IN,
        ReasonTypes.INCLUDE,
        ReasonTypes.INHERIT,
        ReasonTypes.LAZY,
        ReasonTypes.LET,
        ReasonTypes.MODULE,
        ReasonTypes.MUTABLE,
        ReasonTypes.OF,
        ReasonTypes.OPEN,
        ReasonTypes.PRI,
        ReasonTypes.PUB,
        ReasonTypes.RAISE,
        ReasonTypes.REF,
        ReasonTypes.SWITCH,
        ReasonTypes.THIS,
        ReasonTypes.TO,
        ReasonTypes.DOWNTO,
        ReasonTypes.TRY,
        ReasonTypes.TYPE,
        ReasonTypes.VAL,
        ReasonTypes.WHEN,
        ReasonTypes.WHILE,
        ReasonTypes.VIRTUAL
)


/**
 * Extra keywords used as tags on functions - for highlighting
 */
val REASON_KEYWORDS_EXTRA = tokenSetOf(
        ReasonTypes.REC,
        ReasonTypes.NONREC,
        ReasonTypes.WITH
)


interface ReasonBlockLevelStmt : ORBlockLevelStmt {
//    override val BLOCK_LEVEL_STATEMENTS: TokenSet
//        get() = REASON_BLOCK_LEVEL_STATEMENTS

    companion object {
        val REASON_BLOCK_LEVEL_STATEMENTS: TokenSet = tokenSetOf(
                ReasonTypes.DEFINE_EXCEPTION_STMT,
                ReasonTypes.MODULE_DEFINITION_STMT,
                ReasonTypes.BARE_EXPR_STMT,
                ReasonTypes.LET_STMT
        )
    }
}


interface ReasonModuleLevelStmt : ORModuleLevelStmt {
//    override val MODULE_LEVEL_STATEMENTS: TokenSet
//        get() = REASON_MODULE_LEVEL_STATEMENTS

    companion object {
        //        Can be anything at a block level, as well as messing with types
        val REASON_MODULE_LEVEL_STATEMENTS: TokenSet = TokenSet.andSet(
                tokenSetOf(
                        ReasonTypes.EXTERNAL_FUNCTION_DEFINITION_STMT,
                        ReasonTypes.MODULE_TYPE_DEFINITION_STMT,
                        ReasonTypes.ONE_OR_MORE_TYPE_DEFINITION_STMT,
                        ReasonTypes.INCLUDE_MODULE_STMT,
                        ReasonTypes.CLASS_DEFINITION_STMT,
                        ReasonTypes.PPXD_STMT
                ),
                ReasonBlockLevelStmt.REASON_BLOCK_LEVEL_STATEMENTS
        )
    }
}


interface ReasonInterfaceLevelStmt : ORInterfaceLevelStmt {
//    override val INTERFACE_LEVEL_STATMENTS: TokenSet
//        get() = REASON_INTERFACE_LEVEL_STATEMENTS

    companion object {
        val REASON_INTERFACE_LEVEL_STATEMENTS: TokenSet = tokenSetOf(
                ReasonTypes.EXTERNAL_FUNCTION_DEFINITION_STMT,
                ReasonTypes.MODULE_SIGNATURE_LET_STMT,
                ReasonTypes.ONE_OR_MORE_TYPE_DEFINITION_STMT,
                ReasonTypes.DEFINE_EXCEPTION_STMT,
                ReasonTypes.MODULE_TYPE_DEFINITION_STMT,
                ReasonTypes.OPEN_MODULE_STMT,
                ReasonTypes.INCLUDE_MODULE_STMT,
                ReasonTypes.PPXD_STMT
        )
    }
}
