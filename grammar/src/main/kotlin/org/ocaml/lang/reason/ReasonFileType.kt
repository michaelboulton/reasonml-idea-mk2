package org.ocaml.lang.reason

import com.intellij.openapi.fileTypes.LanguageFileType
import com.intellij.openapi.vfs.VirtualFile
import javax.swing.Icon

object ReasonFileType : LanguageFileType(ReasonLanguage) {
    override fun getIcon(): Icon? = ReasonImages.fileIcon

    override fun getName(): String = ReasonLanguage.id

    override fun getDefaultExtension(): String = "re"

    override fun getDescription(): String = "Reason file"

    override fun getCharset(file: VirtualFile, content: ByteArray) = "UTF-8"

    override fun toString(): String = "ReasonFile"
}
