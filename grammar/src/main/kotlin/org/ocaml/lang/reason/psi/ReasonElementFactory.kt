package org.ocaml.lang.reason.psi


import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.util.PsiTreeUtil
import org.ocaml.lang.reason.ReasonFileType


class ReasonElementFactory(private val project: Project) {
    private fun createFile(text: String): PsiFile {
        val name = "dummy.re"
        return PsiFileFactory.getInstance(project).createFileFromText(name, ReasonFileType, text)
    }

    fun createTypeNameElement(name: String): ReasonTypeName {
        val createFile = createFile("type $name = int;")
        return PsiTreeUtil.findChildOfType(createFile, ReasonTypeName::class.java)!!
    }

    fun createModuleDefinitionNameElement(name: String): ReasonModuleName {
        val createFile = createFile("module $name = {};")
        return PsiTreeUtil.findChildOfType(createFile, ReasonModuleName::class.java)!!
    }

    fun createModuleTypeDefinitionNameElement(name: String): ReasonModuleTypeName {
        val createFile = createFile("module type $name = {};")
        return PsiTreeUtil.findChildOfType(createFile, ReasonModuleTypeName::class.java)!!
    }
}
