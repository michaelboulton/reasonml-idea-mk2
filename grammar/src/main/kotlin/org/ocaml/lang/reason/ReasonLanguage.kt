package org.ocaml.lang.reason

import com.intellij.lang.Language

object ReasonLanguage : Language("Reason", "text/reason") {
    override fun isCaseSensitive() = true
}
