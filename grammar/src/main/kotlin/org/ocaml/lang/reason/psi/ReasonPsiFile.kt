package org.ocaml.lang.reason.psi

import com.intellij.psi.FileViewProvider
import org.jetbrains.annotations.NotNull
import org.ocaml.lang.reason.ReasonFileType
import org.ocaml.lang.reason.ReasonLanguage
import org.ocaml.lang.shared.psi.BasePsiFile


class ReasonPsiFile(@NotNull viewProvider: FileViewProvider) : BasePsiFile(viewProvider, ReasonLanguage, ReasonFileType)
