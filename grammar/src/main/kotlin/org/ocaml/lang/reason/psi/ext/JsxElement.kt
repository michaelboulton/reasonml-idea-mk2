package org.ocaml.lang.reason.psi.ext

import org.ocaml.lang.reason.psi.ReasonJsxChildren
import org.ocaml.lang.shared.psi.ORElement

interface JsxElement : ORElement {
    fun getJsxChildren(): ReasonJsxChildren?
}
