package org.ocaml.lang.reason.psi


import org.jetbrains.annotations.NonNls
import org.ocaml.lang.reason.ReasonLanguage
import org.ocaml.lang.shared.psi.BaseTokenType


class ReasonTokenType(@NonNls debugName: String) : BaseTokenType(debugName, ReasonLanguage) {
    override fun toString(): String {
        return "ReasonTokenType." + super.toString()
    }
}
