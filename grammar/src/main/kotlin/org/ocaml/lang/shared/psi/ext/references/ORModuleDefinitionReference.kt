package org.ocaml.lang.shared.psi.ext.references


import com.intellij.openapi.util.TextRange
import org.ocaml.lang.reason.psi.ReasonModuleDefinitionStmt
import org.ocaml.lang.reason.psi.ReasonModuleName
import org.ocaml.lang.reason.psi.ReasonPsiUtil


/**
 * Reference to a module definition
 */
class ORModuleDefinitionReference(referencedFrom: ReasonModuleName, textRange: TextRange) : ORReference<ReasonModuleName>(referencedFrom, textRange) {
    private val moduleName: String = referencedFrom.capitalidentifier.text

    override fun multiResolveElement(): Array<ReasonModuleDefinitionStmt> =
            ReasonPsiUtil.findModuleDefinitionsByName(myElement.project, moduleName).toTypedArray()
}
