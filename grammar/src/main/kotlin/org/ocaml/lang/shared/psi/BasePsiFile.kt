package org.ocaml.lang.shared.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.lang.Language
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider


abstract class BasePsiFile(
        viewProvider: FileViewProvider,
        language: Language,
        private val languageFileType: FileType
) : PsiFileBase(viewProvider, language) {
    override fun getFileType(): FileType = languageFileType

    override fun toString(): String = languageFileType.toString()
}
