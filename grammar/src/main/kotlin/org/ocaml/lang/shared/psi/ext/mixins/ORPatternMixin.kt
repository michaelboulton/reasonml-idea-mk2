package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.psi.PsiElement
import org.ocaml.lang.reason.psi.ReasonValueNamePattern
import org.ocaml.lang.shared.psi.ORElement

/**
 * A Pattern object
 */
interface PatternWithName : ORElement {
}

fun PatternWithName.nameAssignedTo(): PsiElement? = when (this) {
    is ReasonValueNamePattern -> this.identifier
    else -> null
}
