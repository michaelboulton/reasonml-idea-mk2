package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.lang.ASTNode
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import org.ocaml.lang.ocaml.psi.OCamlTypeDefinition
import org.ocaml.lang.reason.psi.ReasonElementFactory
import org.ocaml.lang.reason.psi.ReasonTypeDefinition
import org.ocaml.lang.reason.psi.ReasonTypeName
import org.ocaml.lang.shared.psi.ext.ORNamedElementImpl
import org.ocaml.lang.shared.psi.ext.ORNavigationElement

abstract class ORTypeDefinitionMixin(node: ASTNode) : ORNamedElementImpl(node), ORNavigationElement {
    override fun getPresentation(): ItemPresentation? = ORNavigationElement.getPresentation(this)

    //    FIXME: Make generic
    override fun createNewNameElement(project: Project, newName: String): ReasonTypeName = ReasonElementFactory(this.project).createTypeNameElement(newName)

    override fun getNameIdentifier(): PsiElement? = when (this) {
        is ReasonTypeDefinition -> this.typeName
        is OCamlTypeDefinition -> this.typeName
        else -> null
    }

    override val requiresCapitalName: Boolean get() = false
}
