package org.ocaml.lang.shared.psi

import com.intellij.lang.Language
import com.intellij.psi.tree.IElementType
import org.jetbrains.annotations.NonNls
import org.ocaml.lang.ocaml.OCamlLanguage
import org.ocaml.lang.reason.ReasonLanguage


abstract class BaseTokenType(@NonNls debugName: String, language: Language) : IElementType(debugName, language) {
    override fun toString(): String {
        return language.id + "TokenType." + super.toString()
    }

    class ReasonTokenType(@NonNls debugName: String) : BaseTokenType(debugName, ReasonLanguage)

    class OCamlTokenType(@NonNls debugName: String) : BaseTokenType(debugName, OCamlLanguage)
}
