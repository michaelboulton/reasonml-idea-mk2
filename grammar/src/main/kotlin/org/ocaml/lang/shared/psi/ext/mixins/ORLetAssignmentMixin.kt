package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.lang.ASTNode
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import org.ocaml.lang.ocaml.psi.OCamlLetAssignment
import org.ocaml.lang.reason.psi.*
import org.ocaml.lang.shared.psi.ORModuleName
import org.ocaml.lang.shared.psi.ext.ORNamedElementImpl
import org.ocaml.lang.shared.psi.ext.ORNavigationElement

private val ReasonLetAssignedTo.letAssignIdentifier: PsiElement?
    get() = when (this) {
        is ReasonPatternLetAssignedTo -> this.pattern.nameAssignedTo()
        is ReasonDefineFunLhsLetAssignedTo -> this.identifier
        is ReasonDefineVariableWithTypeHintLetAssignedTo -> null // FIXME: this should be generating a 'getIdentifier' from the bnf, but it isn't
        else -> null
    }

abstract class ORLetAssignmentMixin(node: ASTNode) : ORNamedElementImpl(node), ORNavigationElement {
    override fun getPresentation(): ItemPresentation? = ORNavigationElement.getPresentation(this)

    override fun createNewNameElement(project: Project, newName: String): ORModuleName = ReasonElementFactory(this.project).createModuleDefinitionNameElement(newName)

    override fun getNameIdentifier(): PsiElement? = when (this) {
        is ReasonLetAssignment -> this.letAssignedTo.letAssignIdentifier
        is OCamlLetAssignment -> this.letAssignedTo.identifier
        else -> null
    }

    override val requiresCapitalName: Boolean get() = false
}
