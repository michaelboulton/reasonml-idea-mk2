package org.ocaml.lang.shared.psi.ext

import org.ocaml.lang.reason.psi.ReasonCodeBlock
import org.ocaml.lang.reason.psi.ReasonCodeBlockMaybeWithBrace
import org.ocaml.lang.reason.psi.ReasonModuleContentsContainer
import org.ocaml.lang.reason.psi.ReasonModuleSignatureContentsContainer
import org.ocaml.lang.shared.psi.*

/**
 * A block of code which contains zero or more statements
 *
 * A function, a module, etc.
 *
 * Note that this means there is a hierarchy of, eg,  module -> contents container -> contents
 */
interface ORBlock : ORElement, ORNavigationElement {
    fun <T : ORStmt> getStmtList(): List<T>
}

fun ORBlock.isModuleBlock(): Boolean =
        when (this) {
            is ReasonModuleSignatureContentsContainer, is ReasonModuleContentsContainer -> true
            else -> false
        }


fun ORBlock.isStatementValidIn(statement: ORStmt): Boolean = when (this) {
    is ReasonCodeBlock, is ReasonCodeBlockMaybeWithBrace -> statement is ORBlockLevelStmt
    is ReasonModuleSignatureContentsContainer -> statement is ORInterfaceLevelStmt
    is ReasonModuleContentsContainer -> statement is ORModuleLevelStmt
    else -> true
}


val ORBlock.blockTypeName: String
    get() = when (this) {
        is ReasonCodeBlock, is ReasonCodeBlockMaybeWithBrace -> "code block"
        is ReasonModuleSignatureContentsContainer -> "module signature"
        is ReasonModuleContentsContainer -> "module definition"
        else -> "<unknown block type>"
    }
