package org.ocaml.lang.shared.psi.ext

import com.intellij.navigation.ItemPresentation
import com.intellij.psi.NavigatablePsiElement
import org.ocaml.lang.reason.ReasonImages
import javax.swing.Icon


/**
 * An element which can show up in the tree on the side
 *
 * This is basically all 'definition' statements - classes, modules, types, etc.
 *
 * This will _normally_ also be alongside [ORNamedElement]
 */
interface ORNavigationElement : NavigatablePsiElement {
    companion object {
        fun getPresentation(thisElement: ORNavigationElement): ItemPresentation? = object : ItemPresentation {
            override fun getPresentableText(): String? = (thisElement as? ORNamedElement)?.name
                    ?: thisElement.text

            override fun getLocationString(): String? = thisElement.containingFile.name

            override fun getIcon(unused: Boolean): Icon? = ReasonImages.getIconFor(thisElement)
        }
    }
}
