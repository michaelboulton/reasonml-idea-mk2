package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.lang.ASTNode
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import org.ocaml.lang.ocaml.psi.OCamlModuleTypeDefinitionStmt
import org.ocaml.lang.reason.psi.ReasonElementFactory
import org.ocaml.lang.reason.psi.ReasonModuleTypeDefinitionStmt
import org.ocaml.lang.reason.psi.ReasonModuleTypeName
import org.ocaml.lang.shared.psi.ext.ORNamedElementImpl
import org.ocaml.lang.shared.psi.ext.ORNavigationElement

abstract class ORModuleTypeDefinitionMixin(node: ASTNode) : ORNamedElementImpl(node), ORNavigationElement {
    override fun getPresentation(): ItemPresentation? = ORNavigationElement.getPresentation(this)

    //    FIXME: Change
    override fun createNewNameElement(project: Project, newName: String): ReasonModuleTypeName = ReasonElementFactory(this.project).createModuleTypeDefinitionNameElement(newName)

    override fun getNameIdentifier(): PsiElement? = when (this) {
        is ReasonModuleTypeDefinitionStmt -> this.moduleTypeName
        is OCamlModuleTypeDefinitionStmt -> this.moduleTypeName
        else -> null
    }

    override val requiresCapitalName: Boolean get() = true
}
