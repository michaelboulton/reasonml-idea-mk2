package org.ocaml.lang.shared.psi.ext.references

import com.intellij.codeInsight.lookup.LookupElement
import com.intellij.openapi.util.TextRange
import com.intellij.psi.*
import org.ocaml.lang.shared.psi.ORElement


/**
 * A PSI element which may have one or more references to somewhere else
 */
interface IORReferenceElement : ORElement {
//    override fun getReference(): PsiReference? =
//            ReferenceProvidersRegistry.getReferencesFromProviders(this).firstOrNull()

    override fun getReferences(): Array<out IORReference>
}


/**
 * The actual reference to some other class
 */
interface IORReference : PsiPolyVariantReference {
    /**
     * Resolve the reference to its source
     *
     * Used by [multiResolve]
     */
    fun multiResolveElement(): Array<out PsiElement>
}


abstract class ORReference<T : PsiElement>(element: T, textRange: TextRange) : PsiPolyVariantReferenceBase<T>(element, textRange), IORReference {
    override fun multiResolve(incompleteCode: Boolean): Array<ResolveResult> =
            multiResolveElement().map { PsiElementResolveResult(it) }.toTypedArray()

    override fun getVariants(): Array<LookupElement> = LookupElement.EMPTY_ARRAY
}
