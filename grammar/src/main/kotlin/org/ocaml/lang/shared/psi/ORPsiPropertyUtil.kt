package org.ocaml.lang.shared.psi

import com.intellij.navigation.ItemPresentation
import org.ocaml.lang.reason.ReasonImages
import org.ocaml.lang.reason.psi.ReasonInfixOperator
import javax.swing.Icon


object ORPsiPropertyUtil {

    /**
     * Get operator from custom operator
     */
    @JvmStatic
    fun getOperator(element: ReasonInfixOperator): ItemPresentation {
        return object : ItemPresentation {
            override fun getPresentableText(): String? = element.node.text

            override fun getLocationString(): String? = element.containingFile.name

            override fun getIcon(unused: Boolean): Icon? = ReasonImages.fileIcon
        }
    }
}
