package org.ocaml.lang.shared.psi.ext

import com.intellij.codeInsight.hint.HintManager
import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNameIdentifierOwner
import com.intellij.psi.PsiNamedElement
import org.ocaml.lang.shared.psi.ORElement

/**
 * From docs:
 *
 * "Every element which can be renamed or referenced needs to implement com.intellij.psi.PsiNamedElement interface."
 *
 * This provides the interface for anything which has a name a type, a variable, a module, etc.
 */
interface ORNamedElement : ORElement, PsiNamedElement, PsiNameIdentifierOwner {

    /**
     * A function that creates a new name element, used when renaming the element
     */
    fun createNewNameElement(project: Project, newName: String): ORElement

    val requiresCapitalName: Boolean
}


/**
 * And an abstract class so we have the constructor passed through
 */
abstract class ORNamedElementImpl(node: ASTNode) : ASTWrapperPsiElement(node), ORNamedElement {
    override fun getName(): String? = this.nameIdentifier?.text

    override fun setName(newName: String): PsiElement {
        if (!isValidNameForElement(this, newName)) return this

        return renameElement(this, newName)
    }
}


/**
 * Module names must start with an uppercase. Everything else must start with lowercase.
 */
internal fun isValidNameForElement(element: ORNamedElement, newName: String): Boolean {
    if (element.requiresCapitalName) {
        if (newName[0].isLowerCase()) {
            HintManager.getInstance().showErrorHint(
                    FileEditorManager.getInstance(element.project).selectedTextEditor!!,
                    "Module and Variant names must start with an uppercase character",
                    HintManager.UNDER
            )
            return false
        }
    } else {
        if (newName[0].isUpperCase()) {
            HintManager.getInstance().showErrorHint(
                    FileEditorManager.getInstance(element.project).selectedTextEditor!!,
                    "Type names must start with a lowercase character",
                    HintManager.UNDER
            )
            return false
        }
    }

    return true
}


internal fun renameElement(element: ORNamedElement, newName: String): ORNamedElement {
    element.nameIdentifier
            ?.replace(element.createNewNameElement(element.project, newName))

    return element
}
