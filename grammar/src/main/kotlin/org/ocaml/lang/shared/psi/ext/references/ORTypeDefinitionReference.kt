package org.ocaml.lang.shared.psi.ext.references


import com.intellij.openapi.util.TextRange
import org.ocaml.lang.reason.psi.ReasonPsiUtil
import org.ocaml.lang.reason.psi.ReasonTypeDefinition
import org.ocaml.lang.reason.psi.ReasonUseExistingTypeTypeUse


/**
 * Reference to a type definition
 */
class ORTypeDefinitionReference(referencedFrom: ReasonUseExistingTypeTypeUse, textRange: TextRange) : ORReference<ReasonUseExistingTypeTypeUse>(referencedFrom, textRange) {
    private val typeName: String = referencedFrom.identifier.text

    override fun multiResolveElement(): Array<ReasonTypeDefinition> =
            ReasonPsiUtil.findTypeDefinitionsByName(myElement.project, typeName).toTypedArray()
}
