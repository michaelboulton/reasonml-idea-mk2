package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode
import com.intellij.psi.PsiReference
import org.ocaml.lang.ocaml.psi.OCamlSimpleTypeUse
import org.ocaml.lang.reason.psi.ReasonPsiUtil
import org.ocaml.lang.reason.psi.ReasonUseExistingTypeTypeUse
import org.ocaml.lang.shared.psi.ext.references.ORTypeDefinitionReference


abstract class ORSimpleTypeUseMixin(node: ASTNode) : ASTWrapperPsiElement(node), org.ocaml.lang.shared.psi.ORElement {

    override fun getReferences(): Array<out ORTypeDefinitionReference> {
        val typeName = when (this) {
            is ReasonUseExistingTypeTypeUse -> this.identifier
            is OCamlSimpleTypeUse -> this.typeName
            else -> return emptyArray()
        }

//        FIXME: Make finding type definitons generic
        val typeDefs = ReasonPsiUtil.findTypeDefinitionsByName(this.project, typeName.text)

        return typeDefs
                .map {
                    ORTypeDefinitionReference(this as ReasonUseExistingTypeTypeUse, it.typeName.identifier.textRangeInParent)
                }
                .toTypedArray()
    }

    override fun getReference(): PsiReference? {
        return references.firstOrNull()
    }
}
