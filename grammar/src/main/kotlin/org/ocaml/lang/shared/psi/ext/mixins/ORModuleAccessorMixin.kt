package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode
import com.intellij.psi.PsiReference
import org.ocaml.lang.shared.psi.ext.references.ORModuleDefinitionReference

abstract class ORModuleAccessorMixin(node: ASTNode) : ASTWrapperPsiElement(node) {

    /**
     * FIXME: this finds referneces but then the reverse reference is incorrect because it refers to the _name_, not _this element_.
     *
     * Need to think about how to do these kind of references
     * - should it on moduleName?
     * - should there ba an extra utility function like findModuleDefinitionsInScope?
     *
     * In general, this should only return results _for the last name in the list_
     */
    override fun getReferences(): Array<ORModuleDefinitionReference> {
        return emptyArray()

//        return this.moduleNameList.map { moduleName ->
//            ReasonPsiUtil.findModuleDefinitionsByName(this.project, moduleName.text)
//                    .map { OCamlModuleDefinitionReference(moduleName, moduleName.capitalidentifier.textRangeInParent) }
//        }
//                .flatten()
//                .toTypedArray()
    }

    override fun getReference(): PsiReference? {
        return references.firstOrNull()
    }
}
