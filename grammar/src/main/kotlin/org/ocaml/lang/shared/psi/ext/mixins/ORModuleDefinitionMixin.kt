package org.ocaml.lang.shared.psi.ext.mixins

import com.intellij.lang.ASTNode
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import org.ocaml.lang.ocaml.psi.OCamlModuleDefinitionStmt
import org.ocaml.lang.reason.psi.ReasonElementFactory
import org.ocaml.lang.reason.psi.ReasonModuleDefinitionStmt
import org.ocaml.lang.shared.psi.ORModuleName
import org.ocaml.lang.shared.psi.ext.ORNamedElementImpl
import org.ocaml.lang.shared.psi.ext.ORNavigationElement

abstract class ORModuleDefinitionMixin(node: ASTNode) : ORNamedElementImpl(node), ORNavigationElement {
    override fun getPresentation(): ItemPresentation? = ORNavigationElement.getPresentation(this)

    override fun createNewNameElement(project: Project, newName: String): ORModuleName = ReasonElementFactory(this.project).createModuleDefinitionNameElement(newName)

    override fun getNameIdentifier(): PsiElement? = when (this) {
        is ReasonModuleDefinitionStmt -> this.moduleName
        is OCamlModuleDefinitionStmt -> this.moduleName
        else -> null
    }

    override val requiresCapitalName: Boolean get() = true
}
