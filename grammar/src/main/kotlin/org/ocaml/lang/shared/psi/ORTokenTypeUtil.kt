package org.ocaml.lang.shared.psi


//FIXME: Add methods back in somehow

/**
 * Things which can appear in a code block/a function
 */
interface ORBlockLevelStmt : org.ocaml.lang.shared.psi.ORStmt {
//    val BLOCK_LEVEL_STATEMENTS: TokenSet
}

/**
 * Can appear at the module level
 *
 * Note that for this and [ORInterfaceLevelStmt], there are _variations_ of the statements which are
 * invalid (like certain ways of including modules) - this needs to be done separately
 */
interface ORModuleLevelStmt : org.ocaml.lang.shared.psi.ORStmt {
//    val MODULE_LEVEL_STATEMENTS: TokenSet
}

/**
 * Can appear in an interface/module type
 */
interface ORInterfaceLevelStmt : org.ocaml.lang.shared.psi.ORStmt {
//    val INTERFACE_LEVEL_STATMENTS: TokenSet
}
