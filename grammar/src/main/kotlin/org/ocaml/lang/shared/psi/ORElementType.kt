package org.ocaml.lang.shared.psi

import com.intellij.lang.Language
import com.intellij.psi.tree.IElementType
import org.ocaml.lang.ocaml.OCamlLanguage
import org.ocaml.lang.reason.ReasonLanguage

sealed class ORElementType(debugName: String, language: Language) : IElementType(debugName, language) {
    class OCamlElementType(debugName: String) : ORElementType(debugName, OCamlLanguage)
    class ReasonElementType(debugName: String) : ORElementType(debugName, ReasonLanguage)

}

object ElementFactories {
    @JvmStatic
    fun reasonElementFactory(debugName: String): IElementType {
        return ORElementType.ReasonElementType(debugName)
    }

    @JvmStatic
    fun ocamlElementFactory(debugName: String): IElementType {
        return ORElementType.OCamlElementType(debugName)
    }
}
