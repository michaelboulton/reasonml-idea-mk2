package org.ocaml.lang.shared.psi

import com.intellij.lexer.FlexAdapter
import com.intellij.lexer.FlexLexer

abstract class BaseLexerAdapter(flex: FlexLexer) : FlexAdapter(flex)
