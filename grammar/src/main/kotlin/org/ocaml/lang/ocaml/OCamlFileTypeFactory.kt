package org.ocaml.lang.ocaml

import com.intellij.openapi.fileTypes.ExtensionFileNameMatcher
import com.intellij.openapi.fileTypes.FileTypeConsumer
import com.intellij.openapi.fileTypes.FileTypeFactory

object OCamlFileTypeFactory : FileTypeFactory() {
    override fun createFileTypes(consumer: FileTypeConsumer) {
        consumer.consume(
                OCamlFileType,
                ExtensionFileNameMatcher(OCamlFileType.defaultExtension)
        )
    }
}
