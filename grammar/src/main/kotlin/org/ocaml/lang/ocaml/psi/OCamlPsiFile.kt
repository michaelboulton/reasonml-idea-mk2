package org.ocaml.lang.ocaml.psi

import com.intellij.psi.FileViewProvider
import org.jetbrains.annotations.NotNull
import org.ocaml.lang.ocaml.OCamlFileType
import org.ocaml.lang.ocaml.OCamlLanguage
import org.ocaml.lang.shared.psi.BasePsiFile


class OCamlPsiFile(@NotNull viewProvider: FileViewProvider) : BasePsiFile(viewProvider, OCamlLanguage, OCamlFileType)
