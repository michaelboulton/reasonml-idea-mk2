package org.ocaml.lang.ocaml.psi


import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import org.ocaml.lang.ocaml.OCamlFileType


class OCamlElementFactory(private val project: Project) {
    private fun createFile(text: String): PsiFile {
        val name = "dummy.ml"
        return PsiFileFactory.getInstance(project).createFileFromText(name, OCamlFileType, text)
    }

//    fun createTypeNameElement(name: String): ReasonTypeName {
//        val createFile = createFile("type $name = int;")
//        return PsiTreeUtil.findChildOfType(createFile, ReasonTypeName::class.java)!!
//    }
//
//    fun createModuleDefinitionNameElement(name: String): ReasonModuleName {
//        val createFile = createFile("module $name = {};")
//        return PsiTreeUtil.findChildOfType(createFile, ReasonModuleName::class.java)!!
//    }
//
//    fun createModuleTypeDefinitionNameElement(name: String): ReasonModuleTypeName {
//        val createFile = createFile("module type $name = {};")
//        return PsiTreeUtil.findChildOfType(createFile, ReasonModuleTypeName::class.java)!!
//    }
}
