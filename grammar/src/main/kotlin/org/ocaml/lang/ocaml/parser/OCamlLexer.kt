package org.ocaml.lang.ocaml.parser

import com.intellij.lexer.FlexAdapter
import org.ocaml.lang.shared.lexer._ORLexer
import org.ocaml.lang.shared.psi.OCamlTypeBridge

object OCamlLexer : FlexAdapter(_ORLexer(OCamlTypeBridge))
