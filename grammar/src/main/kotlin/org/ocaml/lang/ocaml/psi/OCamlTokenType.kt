package org.ocaml.lang.ocaml.psi


import org.jetbrains.annotations.NonNls
import org.ocaml.lang.ocaml.OCamlLanguage
import org.ocaml.lang.shared.psi.BaseTokenType


class OCamlTokenType(@NonNls debugName: String) : BaseTokenType(debugName, OCamlLanguage) {
    override fun toString(): String {
        return "ReasonTokenType." + super.toString()
    }
}
