package org.ocaml.lang.ocaml

import com.intellij.openapi.util.IconLoader
import org.ocaml.lang.shared.psi.ext.ORNavigationElement
import org.ocaml.lang.shared.psi.ext.mixins.ORLetAssignmentMixin
import javax.swing.Icon

object OCamlImages {
    fun getIconFor(thisElement: ORNavigationElement): Icon? =
            when (thisElement) {
                is ORLetAssignmentMixin -> IconLoader.getIcon("/nodes/variable.png")
                else -> fileIcon

            }

    val fileIcon = IconLoader.getIcon("/img/file_type_ocaml.svg")
}
