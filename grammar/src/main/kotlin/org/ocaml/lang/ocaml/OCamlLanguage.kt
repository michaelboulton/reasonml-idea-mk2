package org.ocaml.lang.ocaml

import com.intellij.lang.Language

object OCamlLanguage : Language("OCaml", "text/ocaml") {
    override fun isCaseSensitive() = true
}
