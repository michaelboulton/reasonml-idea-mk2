package org.ocaml.lang.ocaml.parser

import org.ocaml.lang.ocaml.psi.OCamlPsiFile
import org.ocaml.lang.reason.psi.ReasonTypes
import org.ocaml.lang.shared.parser.BaseParserDefinition


class OCamlParserDefinition : BaseParserDefinition(
        OCamlLexer,
        OCamlParser(),
        { OCamlPsiFile(it) },
        { ReasonTypes.Factory.createElement(it) }
)
