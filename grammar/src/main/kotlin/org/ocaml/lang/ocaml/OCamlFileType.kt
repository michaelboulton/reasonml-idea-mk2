package org.ocaml.lang.ocaml

import com.intellij.openapi.fileTypes.LanguageFileType
import com.intellij.openapi.vfs.VirtualFile
import javax.swing.Icon

object OCamlFileType : LanguageFileType(OCamlLanguage) {
    override fun getIcon(): Icon? = OCamlImages.fileIcon

    override fun getName(): String = OCamlLanguage.id

    override fun getDefaultExtension(): String = "ml"

    override fun getDescription(): String = "OCaml file"

    override fun getCharset(file: VirtualFile, content: ByteArray) = "UTF-8"

    override fun toString(): String = "OCamlFile"
}
