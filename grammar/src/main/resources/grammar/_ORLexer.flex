package org.ocaml.lang.shared.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.ocaml.lang.shared.psi.ORTypes;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static org.ocaml.lang.shared.psi.ORTypes.*;


%%

%{
  public _ORLexer(ORTypes elementTypes) {
      this((java.io.Reader)null);

      assert (elementTypes != null);
      this.elementTypes = elementTypes;
    }

    private ORTypes elementTypes;
%}

%{}
  /**
  * Matching quotedLiteral string tag
  */
  private String quotedLiteralMatchTag = null;

  /**
  * The same as RAW_STRING_BEGIN, for matching ends of groups
  */
  private Pattern rawStringBeginPattern = Pattern.compile("\\{([a-zA-Z]+)?\\|");
  private Pattern rawStringEndPattern = Pattern.compile("\\|([a-zA-Z]+)?}");
%}

%{
  void setTagMatch() {
      String toMatch = yytext().toString();

      // This is expected to have been reset
//      assert quotedLiteralMatchTag == null;

      Matcher matcher = rawStringBeginPattern.matcher(toMatch);
      // This should only be called if it finds a match
      assert matcher.find();

      if (matcher.groupCount() == 1){
          quotedLiteralMatchTag = matcher.group(1);
      } else {
          // ie, "don't expect any match"
          quotedLiteralMatchTag = "";
      }
  }

  Boolean checkTagMatch() {
      String toMatch = yytext().toString();

      // Should always be set
      assert quotedLiteralMatchTag != null;

      Matcher matcher = rawStringEndPattern.matcher(toMatch);
      assert matcher.find();

      if (matcher.groupCount() == 1) {
          // Some extra tag found

          if(quotedLiteralMatchTag.equals(matcher.group(1))) {
              // Same tag found - good
              quotedLiteralMatchTag = null;
              return true;
          }
          // else ignore and continue
      } else {
          // '|}' found

          if(quotedLiteralMatchTag == "") {
              // No extra tag expected - good
              quotedLiteralMatchTag = null;
              return true;
          }
          // else ignore and continue
      }

      return false;
  }
%}

%public
%class _ORLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

%s IN_BLOCK_COMMENT
%s IN_DOC_BLOCK
%s IN_QUOTED_LITERAL_STRING
%s IN_MULTILINE_STRING


/*************************** SYMBOLS */

DOT = "."
COLON = ":"
QUESTION_MARK = "?"
EQ = "="
PIPE = "|"
DOLLAR = "$"
TILDE = "~"
AMPERSAND = "&"
LT = "<"
GT = ">"
BANG = "!"
CARRET = "^"
PLUS = "+"
MINUS = "-"
SLASH = "/"
TILDE = "~"
STAR = "*"
PERCENT = "%"
ARROBASE = "@"
SHARP = "#"

EOL =\n|\r|\r\n
WHITE_SPACE_CHAR = [ \t\f\x0B]
WHITE_SPACE = ({EOL}|{WHITE_SPACE_CHAR})+

IDENTIFIER=([a-z_][a-zA-Z0-9_']*)
CAPITALIDENTIFIER=([A-Z][a-zA-Z0-9_']*)

// Used in switch/fun/variants - the only place where newlines are syntactically meaningful?
// FIXME: This also matches
//          StringUtil.xor_string(`Hex(str1), `Hex(str2))
//          |> StringUtil.get_raw_hex_string;
ALTERNATIVE={EOL}{WHITE_SPACE_CHAR}*{PIPE}[^.:?=|$~&<>!\^+\-/~*%@#]
//ALTERNATIVE={PIPE}

// simple line comment
COMMENT=("//".*)

/*************************** RANDOM */

// Ocaml 4.9+
LET=let(%\w+)?

/*************************** RANDOM */

// Ocaml 4.9+
LET=let(%\w+)?

/*************************** STRINGS/CHARS */

// Used inside a char or string to insert a literal
//  generic escapes
ESCAPED_GENERIC  =\\[\\|\"|\'|n|t|b|r|[ ]]
//  ascii character
ESCAPED_ASCII_DEC=\\[0-9][0-9][0-9]
//  ascii character in hex
ESCAPED_ASCII_HEX=\\x[0-9a-fA-F][0-9a-fA-F]
//  ascii character in octal
ESCAPED_ASCII_OCT=\\o[0-3][0-7][0-7]

CHAR_ESCAPE=({ESCAPED_GENERIC}|{ESCAPED_ASCII_DEC}|{ESCAPED_ASCII_HEX}|{ESCAPED_ASCII_OCT})

// _one_ element
VALID_CHARACTER=.|{CHAR_ESCAPE}

CHAR='{VALID_CHARACTER}'


UNICODE_STRING_CHAR=\\u\{[0-9a-fA-F]+\}
// NOTE: does not include newlines for multiline strings
STRING_ITEM={VALID_CHARACTER}|{UNICODE_STRING_CHAR}

RAW_STRING_BEGIN=(\{([a-zA-Z_]+)?\|)
RAW_STRING_END=(\|([a-zA-Z_]+)?\})


/*************************** INTEGERS */
// "For convenience and readability, underscore characters (_) are accepted (and ignored) within integer literals."
DEC_LITERAL=[0-9][0-9_]*
HEX_LITERAL=0[xX][0-9A-Fa-f][0-9A-Fa-f_]*
OCT_LITERAL=0[oO][0-7][0-7_]*
BIN_LITERAL=0[bB][0-1][0-1_]*

// An integer literal with one of possible sufixes to indicate the integer width
INTEGER = ({DEC_LITERAL}|{HEX_LITERAL}|{OCT_LITERAL}|{BIN_LITERAL})[lLn]?


/*************************** FLOATS */
DEC_FLOAT=(
//  Standard beginning
    [0-9][0-9_]*
//    Floating point suffix
        (\.[0-9_])?
//    Exponential
        ([eE][+-]?{DEC_LITERAL})?
)
OCT_FLOAT=(
    [0xX][0-9A-Fa-f][0-9A-Fa-f_]*
        (\.[0-9A-Fa-f_]*)?
        ([pP][+-]?{DEC_LITERAL})
)
FLOAT = ({OCT_FLOAT}|{DEC_FLOAT})


%%
<YYINITIAL> {
// Reserved keywords
  "begin"                    { return elementTypes.BEGIN; }
  "do"                       { return elementTypes.DO; }
  "done"                     { return elementTypes.DONE; }
  "end"                      { return elementTypes.END; }
  "endif"                    { return elementTypes.ENDIF; }
  "function"                 { return elementTypes.FUNCTION; }
  "functor"                  { return elementTypes.FUNCTOR; }
  "initializer"              { return elementTypes.INITIALIZER; }
  "new"                      { return elementTypes.NEW; }
  "object"                   { return elementTypes.OBJECT; }
  "or"                       { return elementTypes.OR; }
  "sig"                      { return elementTypes.SIG; }
  "struct"                   { return elementTypes.STRUCT; }
  "then"                     { return elementTypes.THEN; }
  "raw"                      { return elementTypes.RAW; }

// keywords
  "and"                      { return elementTypes.AND; }
  "as"                       { return elementTypes.AS; }
  "assert"                   { return elementTypes.ASSERT; }
  "class"                    { return elementTypes.CLASS_KW; }
  "constraint"               { return elementTypes.CONSTRAINT; }
  "downto"                   { return elementTypes.DOWNTO; }
  "else"                     { return elementTypes.ELSE; }
  "exception"                { return elementTypes.EXCEPTION; }
  "external"                 { return elementTypes.EXTERNAL; }
  "failwith"                 { return elementTypes.FAILWITH; }
  "for"                      { return elementTypes.FOR; }
  "fun"                      { return elementTypes.FUN; }
  "if"                       { return elementTypes.IF; }
  "in"                       { return elementTypes.IN; }
  "include"                  { return elementTypes.INCLUDE; }
  "inherit"                  { return elementTypes.INHERIT; }
  "lazy"                     { return elementTypes.LAZY; }
  {LET}                      { return elementTypes.LET; }
  "module"                   { return elementTypes.MODULE; }
  "mutable"                  { return elementTypes.MUTABLE; }
  "nonrec"                   { return elementTypes.NONREC; }
  "of"                       { return elementTypes.OF; }
  "open"                     { return elementTypes.OPEN; }
  "pub"                      { return elementTypes.PUB; }
  "pri"                      { return elementTypes.PRI; }
  "rec"                      { return elementTypes.REC; }
  "switch"                   { return elementTypes.SWITCH; }
  "this"                     { return elementTypes.THIS; }
  "to"                       { return elementTypes.TO; }
  "try"                      { return elementTypes.TRY; }
  "type"                     { return elementTypes.TYPE; }
  "val"                      { return elementTypes.VAL; }
  "when"                     { return elementTypes.WHEN; }
  "while"                    { return elementTypes.WHILE; }
  "with"                     { return elementTypes.WITH; }
  "()"                       { return elementTypes.UNIT; }
  "ref"                      { return elementTypes.REF; }
  "raise"                    { return elementTypes.RAISE; }

  "mod"                      { return elementTypes.MOD; }
  "land"                     { return elementTypes.LAND; }
  "lor"                      { return elementTypes.LOR; }
  "lxor"                     { return elementTypes.LXOR; }
  "lsl"                      { return elementTypes.LSL; }
  "lsr"                      { return elementTypes.LSR; }
  "asr"                      { return elementTypes.ASR; }

  "false"                    { return elementTypes.BOOL_FALSE; }
  "true"                     { return elementTypes.BOOL_TRUE; }

// Easily mistaken but bad characters
  ";;"                       {return elementTypes.SEMISEMI;}

// special characters
  ","                        { return elementTypes.COMMA; }
  ";"                        { return elementTypes.SEMI; }
  "'"                        { return elementTypes.QUOTE; }
  "..."                      { return elementTypes.DOTDOTDOT; }
  ".."                       { return elementTypes.DOTDOT; }
  "_"                        { return elementTypes.UNDERSCORE; }
  ":="                       { return elementTypes.COLON_EQ; }
  "`"                        { return elementTypes.BACKTICK; }

// special meta-characters
  "/>"                       { return elementTypes.JSXTAGEND; }
  "</"                       { return elementTypes.JSXTAGBEGIN; }
  "<>"                       { return elementTypes.JSXFRAGMENTBEGIN; }
  "</>"                      { return elementTypes.JSXFRAGMENTEND; }
  "[@"                       { return elementTypes.PPXTAGOPEN; }
  "("                        { return elementTypes.LPAREN; }
  ")"                        { return elementTypes.RPAREN; }
  "{"                        { return elementTypes.LBRACE; }
  "}"                        { return elementTypes.RBRACE; }
  "["                        { return elementTypes.LBRACKET; }
  "]"                        { return elementTypes.RBRACKET; }
  "=>"                       { return elementTypes.ARROW; }
  "[|"                       { return elementTypes.LARRAY; }
  "|]"                       { return elementTypes.RARRAY; }
  "[<"                       { return elementTypes.BRACKET_LT; }
  "[>"                       { return elementTypes.BRACKET_GT; }
  ":>"                       { return elementTypes.COLON_GT; }

// Matches a pipe on a newline - needs to come before the next block so it gets precedence over PIPE
  {ALTERNATIVE}              { return elementTypes.ALTERNATIVE; }

// Single characters which can be used in operators
// Note: some of these have other meanings as well, parser handles contextual meaning.
  {DOT}                      { return elementTypes.DOT; }
  {COLON}                    { return elementTypes.COLON; }
  {EQ}                       { return elementTypes.EQ; }
  {PIPE}                     { return elementTypes.PIPE; }
  {ARROBASE}                 { return elementTypes.ARROBASE; }
  {SHARP}                    { return elementTypes.SHARP; }
  {QUESTION_MARK}            { return elementTypes.QUESTION_MARK; }
  {DOLLAR}                   { return elementTypes.DOLLAR; }
  {TILDE}                    { return elementTypes.TILDE; }
  {AMPERSAND}                { return elementTypes.AMPERSAND; }
  {LT}                       { return elementTypes.LT; }
  {GT}                       { return elementTypes.GT; }
  {BANG}                     { return elementTypes.BANG; }
  {CARRET}                   { return elementTypes.CARRET; }
  {PLUS}                     { return elementTypes.PLUS; }
  {MINUS}                    { return elementTypes.MINUS; }
  {SLASH}                    { return elementTypes.SLASH; }
  {STAR}                     { return elementTypes.STAR; }
  {PERCENT}                  { return elementTypes.PERCENT; }

//      Multi-character operators
//  "&&"                       { return elementTypes.AMPERAMPER; }
//  "##"                       { return elementTypes.SHARPSHARP; }
//  "->"                       { return elementTypes.RIGHT_ARROW; }
//  "<-"                       { return elementTypes.LEFT_ARROW; }
//  "|>"                       { return elementTypes.PIPE_FORWARD; }
//  "<|"                       { return elementTypes.PIPE_BACKWARD; }
//  "==="                      { return elementTypes.EQEQEQ; }
//  "=="                       { return elementTypes.EQEQ; }
//  "!=="                      { return elementTypes.NOT_EQEQ; }
//  "!="                       { return elementTypes.NOT_EQ; }
//  "<="                       { return elementTypes.LT_OR_EQUAL; }
//  ">="                       { return elementTypes.GT_OR_EQUAL; }
//  "++"                       { return elementTypes.PLUSPLUS; }
//  "+."                       { return elementTypes.PLUSDOT; }
//  "-."                       { return elementTypes.MINUSDOT; }
//  "/."                       { return elementTypes.SLASHDOT; }
//  "*."                       { return elementTypes.STARDOT; }
//  "**"                       { return elementTypes.STARSTAR; }

//  Other things
  {IDENTIFIER}               { return elementTypes.IDENTIFIER; }
  {CAPITALIDENTIFIER}        { return elementTypes.CAPITALIDENTIFIER; }
  {COMMENT}                  { return elementTypes.COMMENT; }
  {INTEGER}                  { return elementTypes.INTEGER; }
  {FLOAT}                    { return elementTypes.FLOAT; }
  {CHAR}                     { return elementTypes.CHAR; }

  // order matters here
  "/**"                      { yybegin(IN_DOC_BLOCK);
                               yypushback(3);
                             }
  "/*"                       { yybegin(IN_BLOCK_COMMENT);
                               yypushback(2);
                             }

  {RAW_STRING_BEGIN}         { yybegin(IN_QUOTED_LITERAL_STRING);
                               setTagMatch(); }
  \"                         { yybegin(IN_MULTILINE_STRING); }

  {WHITE_SPACE}              { return WHITE_SPACE; }
}

// Go forever until the first */ is matches
// https://westes.github.io/flex/manual/How-can-I-match-C_002dstyle-comments_003f.html
<IN_BLOCK_COMMENT>{
  "*/" |
  <<EOF>>   { yybegin(YYINITIAL);
              return elementTypes.BLOCK_COMMENT; }

  [^]     { }
}

// Separate because had trouble just getting them to both work in one block
// TODO: odoc/javadoc
<IN_DOC_BLOCK>{
  "*/" |
  <<EOF>>   { yybegin(YYINITIAL);
              return elementTypes.DOC_BLOCK; }

  [^]     { }
}

// Inside a quotedLiteral string
<IN_QUOTED_LITERAL_STRING>{
  {RAW_STRING_END}    { if (checkTagMatch()) {
                            yybegin(YYINITIAL);
                            return elementTypes.STRING;
                        }
                      }

  // eg, Accidentally non-matched quotedLiteral string like '{thing| fkfkfdkod |thin}' <- missing 'g'
  <<EOF>>   { yybegin(YYINITIAL);
              return BAD_CHARACTER; }

  {STRING_ITEM}     { }
}

// In a simple multiline string
<IN_MULTILINE_STRING>{
  \"        { yybegin(YYINITIAL);
              return elementTypes.STRING; }

  <<EOF>>   { yybegin(YYINITIAL);
              return BAD_CHARACTER; }

//  Starting a new line of the multiline string
  \\{EOL}            { }
//  A valid string character
  {STRING_ITEM}      { }
}
[^] { return BAD_CHARACTER; }

    //[^]                              { throw new Error("Illegal character <"+ yytext()+">"); }
