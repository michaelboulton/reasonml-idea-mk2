package org.ocaml.lang.reason.parser

import org.ocaml.lang.shared.parser.ParsingTestBase

class ReasonParsingTest : ParsingTestBase("re", ReasonParserDefinition()) {
    fun `test let`() = doTest(true)
    fun `test module`() = doTest(true)

    fun `test simple string`() = doTest(true)

    fun `test simple type`() = doTest(true)
    fun `test object type`() = doTest(true)
    fun `test record type`() = doTest(true)
    fun `test variant type`() = doTest(true)

    fun `test quotes`() = doTest(true)
}
