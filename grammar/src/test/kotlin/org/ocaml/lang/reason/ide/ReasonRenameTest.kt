package org.ocaml.lang.reason.ide

import org.ocaml.lang.shared.parser.RenameTestBase

class ReasonRenameTest : RenameTestBase() {
    fun `test rename a type`() = doTest(
            "thing_t",
            """
        type blkop/*caret*/t = int;
        let bl: blkopt = 3;
    """,
            """
        type thing_t = int;
        let bl: thing_t = 3;
    """
    )
}
