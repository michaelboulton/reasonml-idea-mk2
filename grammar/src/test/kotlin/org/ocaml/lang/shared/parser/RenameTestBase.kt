package org.ocaml.lang.shared.parser

import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.intellij.lang.annotations.Language


abstract class RenameTestBase : BasePlatformTestCase() {

    inner class InlineFile(@Language("Reason") private val code: String, val name: String = "main.re") {
        private val hasCaretMarker = "/*caret*/" in code

        init {
            myFixture.configureByText(name, replaceCaretMarker(code))
        }

        private fun replaceCaretMarker(text: String) = text.replace("/*caret*/", "<caret>")

        fun withCaret() {
            check(hasCaretMarker) {
                "Please, add `/*caret*/` marker to\n$code"
            }
        }
    }

    internal fun doTest(
            newName: String,
            @Language("Rust") before: String,
            @Language("Rust") after: String
    ) {
        InlineFile(before).withCaret()
        val element = myFixture.elementAtCaret
        myFixture.renameElement(element, newName, true, true)
        myFixture.checkResult(after)
    }
}

