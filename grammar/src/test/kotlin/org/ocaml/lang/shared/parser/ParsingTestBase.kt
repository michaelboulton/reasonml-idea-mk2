package org.ocaml.lang.shared.parser


import com.intellij.testFramework.ParsingTestCase

/**
 * Base class for parses tests for whatever language
 *
 * define a function like "fun `test a test`" and it will check a_test.re matches a_test.txt psi
 */
abstract class ParsingTestBase(fileExt: String, parserDefinition: BaseParserDefinition) :
        ParsingTestCase("/parser/$fileExt", fileExt, parserDefinition) {

    override fun getTestDataPath(): String = "src/test/resources/"

    override fun skipSpaces(): Boolean = false

    override fun includeRanges(): Boolean = true

    /**
     * From rust example
     */
    private fun camelOrWordsToSnake(name: String): String {
        if (' ' in name) return name.trim().replace(" ", "_")

        return name.split("(?=[A-Z])".toRegex()).joinToString("_", transform = String::toLowerCase)
    }

    override fun getTestName(lowercaseFirstLetter: Boolean): String {
        val camelCase = super.getTestName(lowercaseFirstLetter)
        return camelOrWordsToSnake(camelCase)
    }
}
