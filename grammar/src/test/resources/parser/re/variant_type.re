type option('a) = None | Some('a)

type myResponseVariant =
  | Yes
  | No
  | PrettyMuch;

type number('a) =
  | Int(int): number(int)
  | Char(char): number(char);

type polymorphicVariant = [`A | `B];

type polymorphicVariant2 = [
  | `A
  | `B
  ];
