type tesla = {
  .
  color: string
};

type polyopen('b) = {
  .
  color: string,
  f: 'b
};

type car('a) = {
  ..
  color: string
} as 'a;
