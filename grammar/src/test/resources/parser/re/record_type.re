type person = {
  age: int,
  name: string
};

type trailing = {
  location: coord3d,
}

type poly('a) = {
  location: coord3d,
  thing: 'a
}
