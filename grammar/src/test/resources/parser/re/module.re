module MyModule = {
  let message = "hello";
};

module MyModule = {
  module NestedModule = {
    let message = "hello";
  };
};

module type EstablishmentType = {
  type profession;
  let getProfession: profession => string;
};

module MakeSet: MakeSetType = (Item: Comparable) => { };

module type ActualComponent = {
  include BaseComponent;
  let render: unit => string;
};

module type MyList = {
  include (module type of List);
};
