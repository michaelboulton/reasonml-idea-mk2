type scoreType = int;

type coord3d = (float, float, float);

type poly('a) = ('a, float);

type list('a) = list('a, float);

type nested('a) = ('a, list(float));

type nested2('a) = list(list('a), list(float));
