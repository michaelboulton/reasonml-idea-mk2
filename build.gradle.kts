import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    idea
    java
    kotlin("jvm") version "1.4.0"
    id("org.jetbrains.grammarkit") version "2019.3"
    id("org.jetbrains.intellij") version "0.4.21"
}

val pluginGroup: String by project
val pluginName: String by project
val pluginVersion: String by project
val pluginSinceBuild: String by project
val pluginUntilBuild: String by project

val platformType: String by project
val platformDownloadSources: String by project

group = pluginGroup
version = pluginVersion


allprojects {
    repositories {
        mavenCentral()
        jcenter()
        maven("https://dl.bintray.com/jetbrains/markdown")
    }

    plugins.withType<org.jetbrains.grammarkit.GrammarKit> {
        configure<org.jetbrains.grammarkit.GrammarKitPluginExtension> {
            // version of IntelliJ patched JFlex (see bintray link below), Default is 1.7.0-1
            jflexRelease = "1.7.0-1"

            // tag or short commit hash of Grammar-Kit to use (see link below). Default is 2020.1
            grammarKitRelease = "2019.3"
        }
    }

    plugins.withType<org.jetbrains.intellij.IntelliJPlugin> {
        configure<org.jetbrains.intellij.IntelliJPluginExtension> {
            type = platformType
            downloadSources = platformDownloadSources.toBoolean()
            updateSinceUntilBuild = true

            pluginName = "reasonml-idea"
//            this.setPlugins("java")
//    2019.2
            version = "2019.3"
//    version = "191.6707.61"
//    version = "183.6156.11"
//    version = "193"
        }
    }

    tasks {
        withType<Test> {
            useJUnitPlatform()
            testLogging {
                exceptionFormat = TestExceptionFormat.FULL
            }
        }

        withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
            kotlinOptions {
                jvmTarget = JavaVersion.VERSION_11.toString()
                languageVersion = "1.4"
                apiVersion = "1.4"
            }
        }
    }

    afterEvaluate {
        tasks {
            configure<JavaPluginConvention> {
                sourceCompatibility = JavaVersion.VERSION_11
                targetCompatibility = JavaVersion.VERSION_11
            }
        }
    }
}


tasks {
    patchPluginXml {
        setVersion(project.version)
        sinceBuild(pluginSinceBuild)
        untilBuild(pluginUntilBuild)
    }

    runIde.configure {
//        dependsOn(test)
    }
}

val junit5version = "5.5.2"

dependencies {
    implementation(kotlin("stdlib", "1.4.0"))

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junit5version")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit5version")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:$junit5version")
    testImplementation("org.assertj:assertj-core:3.13.2")

    testCompileOnly("junit:junit:4.12") {
        because("allows JUnit 3 and JUnit 4 tests to run")
    }

    implementation(project(mapOf(
            "path" to ":grammar"
    )))
}
