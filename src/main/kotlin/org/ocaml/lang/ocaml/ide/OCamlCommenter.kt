package org.ocaml.lang.ocaml.ide

import com.intellij.lang.Commenter

object OCamlCommenter : Commenter {
    override fun getLineCommentPrefix(): String? = null

    override fun getBlockCommentPrefix(): String? = "(*"

    override fun getBlockCommentSuffix(): String? = "*)"

    override fun getCommentedBlockCommentPrefix(): String? = "(*"

    override fun getCommentedBlockCommentSuffix(): String? = "*)"
}
