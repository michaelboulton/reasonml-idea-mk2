package org.ocaml.lang.reason.ide

import com.intellij.lang.BracePair
import com.intellij.lang.PairedBraceMatcher
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IElementType
import org.ocaml.lang.reason.psi.ReasonTypes


object ReasonBraceMatcher : PairedBraceMatcher {

    //    Structural code elements
    private val structuralPairs =
            mapOf(
                    ReasonTypes.LBRACE to ReasonTypes.RBRACE,
                    ReasonTypes.LBRACKET to ReasonTypes.RBRACKET,
                    ReasonTypes.LPAREN to ReasonTypes.RPAREN,
                    ReasonTypes.LARRAY to ReasonTypes.RARRAY,
                    ReasonTypes.JSXTAGBEGIN to ReasonTypes.JSXTAGEND,
                    ReasonTypes.JSXFRAGMENTBEGIN to ReasonTypes.JSXFRAGMENTEND
            )

    //    Simple matches - eg start/end multiline string
    private val nonStructuralPairs =
            mapOf(
//                    TODO: Matching multiline strings?
//                    ReasonTypes.ML_STRING_OPEN to ReasonTypes.ML_STRING_CLOSE,
//                    ReasonTypes.JS_STRING_OPEN to ReasonTypes.JS_STRING_CLOSE,
                    ReasonTypes.BRACKET_LT to ReasonTypes.RBRACKET,
                    ReasonTypes.BRACKET_GT to ReasonTypes.RBRACKET,
                    ReasonTypes.PPXTAGOPEN to ReasonTypes.RBRACKET
            )

    private val bracePairs: Array<BracePair> by lazy {
        structuralPairs.map { BracePair(it.key, it.value, true) }.toTypedArray() +
                nonStructuralPairs.map { BracePair(it.key, it.value, false) }.toTypedArray()
    }

    override fun getPairs(): Array<BracePair> = bracePairs

    override fun isPairedBracesAllowedBeforeType(braceType: IElementType, contextType: IElementType?): Boolean = true

    override fun getCodeConstructStart(file: PsiFile, openingBraceOffset: Int): Int = openingBraceOffset

}
