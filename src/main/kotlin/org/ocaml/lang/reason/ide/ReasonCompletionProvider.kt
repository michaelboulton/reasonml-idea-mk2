package org.ocaml.lang.reason.ide

import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.patterns.PlatformPatterns
import com.intellij.util.ProcessingContext
import org.ocaml.lang.reason.ReasonLanguage
import org.ocaml.lang.reason.psi.ReasonTypes

object ReasonCompletionProvider : CompletionContributor() {
    init {
        extend(CompletionType.BASIC,
                PlatformPatterns.psiElement(ReasonTypes.VAL).withLanguage(ReasonLanguage),
                object : CompletionProvider<CompletionParameters>() {
                    public override fun addCompletions(
                            parameters: CompletionParameters,
                            context: ProcessingContext,
                            resultSet: CompletionResultSet
                    ) {
//                        FIXME make better
                        resultSet.addElement(LookupElementBuilder.create("Hello"))
                    }
                }
        )
    }
}
