package org.ocaml.lang.reason.ide


import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.options.colors.AttributesDescriptor
import com.intellij.openapi.options.colors.ColorDescriptor
import com.intellij.openapi.options.colors.ColorSettingsPage
import org.ocaml.lang.reason.ReasonImages
import org.ocaml.lang.reason.ReasonLanguage
import org.ocaml.lang.shared.ide.SharedColours
import javax.swing.Icon

object ReasonColourSettingsPage : ColorSettingsPage {

    private val additionalTags = mutableMapOf(
            "BinaryOperator" to SharedColours.OPERATOR.textAttributesKey,
            "VariantName" to SharedColours.VARIANT_NAME.textAttributesKey,
            "ModuleTypeName" to SharedColours.MODULE_TYPE_NAME.textAttributesKey,
            "TypeName" to SharedColours.TYPE_NAME.textAttributesKey
    )

    override fun getAdditionalHighlightingTagToDescriptorMap(): MutableMap<String, TextAttributesKey> = additionalTags

    override fun getIcon(): Icon? = ReasonImages.fileIcon

    override fun getHighlighter(): SyntaxHighlighter = ReasonSyntaxHighlighter

    //    TODO: rust plugin loads this from a file - maybe better
    override fun getDemoText(): String = """
open Common.Extra;
        
// A type
type <TypeName>char_with_rank_t</TypeName> = {
    letter: char,
    ranking: float,
};

type <TypeName>variant_t</TypeName> =
    | <VariantName>Thing1</VariantName>
    | <VariantName>Thing2</VariantName>;

type <TypeName>polyvariant_t</TypeName> =
    | <VariantName>`A</VariantName>
    | <VariantName>`B</VariantName>;

/* A function */
let get_best_char = (acc, next) =>
    if (next.ranking > acc.ranking) {
      next;
    } else {
      acc;
    };

    let ranks =
    List.map(map_ranking, to_try)
    |> List.fold_left(get_best_char, {ranking: (-0.1), letter: 'f'});
    
    ranks.letter;
};

/** Documentation block for module */
module type <ModuleTypeName>SetType</ModuleTypeName> = {
  type <TypeName>profession</TypeName>;
  let getProfession: <TypeName>profession</TypeName> => string;
};

module MakeSet = (Item: Comparable): <ModuleTypeName>SetType</ModuleTypeName> => {
  include BaseComponent;
  let render: <TypeName>unit</TypeName> => <TypeName>string</TypeName>;
};

// operator
let (<BinaryOperator>+~</BinaryOperator>) = a => a <BinaryOperator>!=</BinaryOperator> 3;

let gf = a <BinaryOperator>+~</BinaryOperator> 3;

let blitted = "a" <BinaryOperator>^ </BinaryOperator>"b";

let container =
  <div className="container">
    <table className="table table-hover table-striped test-data">
      <tbody> ...rows </tbody>
    </table>
    <span className="preloadicon glyphicon glyphicon-remove" ariaHidden=true />
  </div>;
    """.trimIndent()

    override fun getAttributeDescriptors(): Array<AttributesDescriptor> = DESCRIPTORS

    override fun getColorDescriptors(): Array<ColorDescriptor> = ColorDescriptor.EMPTY_ARRAY

    override fun getDisplayName(): String = ReasonLanguage.displayName

    private val DESCRIPTORS = SharedColours.values().map { it.attributesDescriptor }.toTypedArray()
}
