package org.ocaml.lang.reason.ide.formatter

import com.intellij.formatting.*
import com.intellij.lang.ASTNode
import com.intellij.psi.formatter.common.AbstractBlock
import org.ocaml.lang.reason.ide.formatter.ReasonBlock.Companion.createReasonBlock
import org.ocaml.lang.reason.psi.ReasonTypes


/**
 * Corresponds to a block of Reason code
 */
class ReasonBlock constructor(
        node: ASTNode,
        wrap: Wrap?,
        alignment: Alignment?,
        val formatContextContainer: FormatContextContainer
) : AbstractBlock(node, wrap, alignment) {

    private val cachedChildren: List<Block> by lazy { this.buildChildrenRec(myNode, emptyList()) }

    override fun buildChildren(): List<Block> = cachedChildren

    override fun getIndent(): Indent? = Indent.getNoneIndent()

    override fun getSpacing(child1: Block?, child2: Block): Spacing? = this.buildSpacing(child1, child2, formatContextContainer)

    override fun isLeaf(): Boolean = myNode.firstChildNode == null

    companion object {
        /**
         * Create a block from an element
         */
        fun createReasonBlock(node: ASTNode, wrap: Wrap?, alignment: Alignment?, formatContextContainer: FormatContextContainer): ReasonBlock {
            return ReasonBlock(
                    node,
                    wrap,
                    alignment,
                    formatContextContainer
            )
        }
    }
}


// TODO: is white_space_char ever returned?
private val ASTNode?.isEmpty: Boolean
    get() = this?.let { this.textLength == 0 || this.elementType in setOf(com.intellij.psi.TokenType.WHITE_SPACE, ReasonTypes.WHITE_SPACE, ReasonTypes.WHITE_SPACE_CHAR) }
            ?: false


/**
 * Spacing for a block between two children
 */
private fun ReasonBlock.buildSpacing(child1: Block?, child2: Block, formatContextContainer: FormatContextContainer): Spacing? {
    return formatContextContainer.spacingBuilder.getSpacing(this, child1, child2)
}


/**
 * Recursively build children
 */
private tailrec fun ReasonBlock.buildChildrenRec(node: ASTNode?, blocks: List<Block>): List<Block> {
    if (node == null) {
        return blocks
    }

    if (node.isEmpty) {
        return buildChildrenRec(node.treeNext, blocks)
    }

    val block = createReasonBlock(
            node,
            Wrap.createWrap(WrapType.NONE, false),
            Alignment.createAlignment(),
            formatContextContainer
    )
    return buildChildrenRec(node.treeNext, blocks + block)
}
