package org.ocaml.lang.reason.ide.formatter


import com.intellij.formatting.FormattingModel
import com.intellij.formatting.FormattingModelBuilder
import com.intellij.formatting.FormattingModelProvider
import com.intellij.formatting.SpacingBuilder
import com.intellij.lang.ASTNode
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.codeStyle.CodeStyleSettings
import org.ocaml.lang.reason.ide.formatter.ReasonBlock.Companion.createReasonBlock


/**
 * Create formatter for Reason
 *
 * This is unconfigurable and should follow refmt exactly
 */
class ReasonFormatModelBuilder : FormattingModelBuilder {
    override fun getRangeAffectingIndent(file: PsiFile?, offset: Int, elementAtOffset: ASTNode?): TextRange? = null

    override fun createModel(element: PsiElement, settings: CodeStyleSettings): FormattingModel {
        TODO("Currently broken")
        return FormattingModelProvider
                .createFormattingModelForPsiFile(
                        element.containingFile,
                        createReasonBlock(element.node, null, null, FormatContextContainer(settings)),
                        settings
                )
    }
}


/**
 * Common stuff that is used in spacing/formatting stuff that can be passed around
 */
data class FormatContextContainer(
        val settings: CodeStyleSettings,
        val spacingBuilder: SpacingBuilder = ReasonSpacingBuilder(settings)
)
