package org.ocaml.lang.reason.ide

import com.intellij.lang.Commenter

object ReasonCommenter : Commenter {
    override fun getLineCommentPrefix(): String? = "//"

    override fun getBlockCommentPrefix(): String? = "/*"

    override fun getBlockCommentSuffix(): String? = "*/"

    override fun getCommentedBlockCommentPrefix(): String? = "/*"

    override fun getCommentedBlockCommentSuffix(): String? = "*/"
}
