package org.ocaml.lang.reason.ide

import com.intellij.lexer.Lexer
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.tree.IElementType
import org.ocaml.lang.reason.parser.ReasonLexer
import org.ocaml.lang.reason.psi.REASON_KEYWORDS
import org.ocaml.lang.reason.psi.REASON_KEYWORDS_EXTRA
import org.ocaml.lang.reason.psi.ReasonTypes
import org.ocaml.lang.shared.ide.SharedColours

object ReasonSyntaxHighlighter : SyntaxHighlighterBase() {

    override fun getHighlightingLexer(): Lexer = ReasonLexer

    override fun getTokenHighlights(tokenType: IElementType): Array<TextAttributesKey> =
            pack(typeToColour(tokenType)?.textAttributesKey)

    private fun typeToColour(tokenType: IElementType): SharedColours? = when (tokenType) {
        ReasonTypes.INFIX_OPERATOR -> SharedColours.OPERATOR
        ReasonTypes.COMMENT, ReasonTypes.BLOCK_COMMENT -> SharedColours.COMMENT
        in arrayOf(ReasonTypes.NAMED_JSX_TAG_EXPR, ReasonTypes.JSX_FRAGMENT_EXPR) -> SharedColours.JSX
        ReasonTypes.IDENTIFIER -> SharedColours.IDENTIFIER
        ReasonTypes.STRING -> SharedColours.STRING
//        Note: when parsing, this could be a module name, or a module type, or a variant, or a polymorphic variant. Because this is lexer-level, we don't know, so just use class name here. This will be annotated appropriately later on.
        ReasonTypes.CAPITALIDENTIFIER -> SharedColours.MODULE_NAME

        ReasonTypes.DOC_BLOCK -> SharedColours.DOC_BLOCK

        in REASON_KEYWORDS -> SharedColours.KEYWORD
        in REASON_KEYWORDS_EXTRA -> SharedColours.KEYWORD_EXTRA

        ReasonTypes.RPAREN, ReasonTypes.LPAREN -> SharedColours.PAREN
        ReasonTypes.RBRACE, ReasonTypes.LBRACE -> SharedColours.BRACE
        ReasonTypes.RARRAY, ReasonTypes.LARRAY -> SharedColours.ARRAY
        ReasonTypes.RBRACKET, ReasonTypes.LBRACKET -> SharedColours.BRACKET

        else -> null
    }

}


object ReasonSyntaxHighlighterFactory : SyntaxHighlighterFactory() {
    override fun getSyntaxHighlighter(project: Project?, virtualFile: VirtualFile?): SyntaxHighlighter =
            ReasonSyntaxHighlighter
}
