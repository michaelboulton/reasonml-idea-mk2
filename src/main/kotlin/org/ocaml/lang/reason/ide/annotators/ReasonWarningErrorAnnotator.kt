package org.ocaml.lang.reason.ide.annotators

import com.intellij.codeInsight.daemon.impl.HighlightRangeExtension
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import org.ocaml.lang.reason.psi.*
import org.ocaml.lang.shared.ide.ORDiagnostics
import org.ocaml.lang.shared.ide.addToHolder
import org.ocaml.lang.shared.psi.ORStmt
import org.ocaml.lang.shared.psi.ext.ORBlock
import org.ocaml.lang.shared.psi.ext.isStatementValidIn

object ReasonErrorAnnotator : Annotator, HighlightRangeExtension {
    override fun isForceHighlightParents(file: PsiFile): Boolean = file is ReasonPsiFile

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        val visitor = object : ReasonVisitor() {
            override fun visitORBlock(o: ORBlock) = checkBlockContents(o, holder)
            override fun visitListLiteralExpr(o: ReasonListLiteralExpr) = checkListType(o, holder)
            override fun visitArrayLiteralExpr(o: ReasonArrayLiteralExpr) = checkArrayType(o, holder)
            override fun visitModuleDefinitionStmt(o: ReasonModuleDefinitionStmt) = checkModuleOverridden(o, holder)
            override fun visitTypeDefinition(o: ReasonTypeDefinition) = checkTypeDefinition(o, holder)
        }

        element.accept(visitor)
    }

    private fun checkTypeDefinition(o: ReasonTypeDefinition, holder: AnnotationHolder) {
        checkInvalidTypeName(o, holder)
    }


    private fun checkBlockContents(block: ORBlock, holder: AnnotationHolder) {
        BlockStatementChecker(block, holder).checkAll()
    }

    private fun checkArrayType(element: ReasonArrayLiteralExpr, holder: AnnotationHolder) {
        checkListOrArrayType(element.exprList, holder)
    }

    private fun checkListType(element: ReasonListLiteralExpr, holder: AnnotationHolder) {
        checkListOrArrayType(element.exprList, holder)
    }

    private fun checkModuleOverridden(element: ReasonModuleDefinitionStmt, holder: AnnotationHolder) {
        ModuleScopeFinder(element, holder).findOverrides()
    }
}


/**
 * Type names should start with a lowercase. presence of capitalidentifier indicates that it's incorrect
 */
private fun checkInvalidTypeName(o: ReasonTypeDefinition, holder: AnnotationHolder) {
    if (!o.typeName.text[0].isLowerCase()) {
        ORDiagnostics.BadTypeNameError(o.typeName).addToHolder(holder)
    }
}

/**
 * Checks to see if some statements are valid in the scope they're used in
 *
 * TODO: Also things like include statements
 */
class BlockStatementChecker(private val block: ORBlock, private val holder: AnnotationHolder) {
    private val statements = block.getStmtList<ORStmt>()

    private fun annotateWrongScope(element: ORStmt) {
        ORDiagnostics.StatementScopeError(element, block).addToHolder(holder)
    }

    fun checkAll() {
        statements.map {
            if (!block.isStatementValidIn(it)) {
                annotateWrongScope(it)
            }
        }
    }
}


enum class ReasonType {
    ReasonBool, ReasonChar, ReasonFloat, ReasonString, ReasonOther;

    override fun toString(): String {
        val superString = super.toString()

        return superString.removePrefix("Reason")
    }
}

/**
 * TODO: Move to a more appropriate utility file
 */
val ReasonExpr.inferredType: ReasonType
    get() {
        return if (this is ReasonConstantExpr) {
            when {
                boolFalse != null -> ReasonType.ReasonBool
                boolTrue != null -> ReasonType.ReasonBool
                char != null -> ReasonType.ReasonChar
                integer != null -> ReasonType.ReasonFloat
                float != null -> ReasonType.ReasonFloat
                string != null -> ReasonType.ReasonString
                else -> throw IllegalStateException("Constant should have been one of the above")
            }
        } else {
            ReasonType.ReasonOther
        }
    }


private fun checkListOrArrayType(exprs: List<ReasonExpr>, holder: AnnotationHolder) {
    val firstElement = exprs.getOrNull(0) ?: return
    exprs.subList(1, exprs.size).map {
        if (it.inferredType != firstElement.inferredType) {
            ORDiagnostics.ListTypeMismatchError(firstElement, it).addToHolder(holder)
        }
    }
}


/**
 * Find overrides of a given module and highlight it
 *
 * TODO: apply to variables/module types/etc as well
 * TODO: Make it scan all scopes rather than just one at a time
 */
class ModuleScopeFinder(private val element: ReasonModuleDefinitionStmt, private val holder: AnnotationHolder) {
    private tailrec fun recurseFindOverrides(elementContainer: ORBlock) {
        val maybeErrorModule = elementContainer.getStmtList<ORStmt>()
                .filterIsInstance<ReasonModuleDefinitionStmt>()
//                Only care about previous definitions
                .takeWhile { modDef -> modDef != element }
//                Which have the same name
                .firstOrNull { modDef -> modDef.name == element.name }
                ?.let {
                    //                    Then highlight this element
                    ORDiagnostics.OveriddenModuleNameWarning(element.moduleName).addToHolder(holder)
                    it
                }

        if (maybeErrorModule != null) return

//        Then check any extra ones
        val parentScope = tryToGetParentScope(elementContainer)
        if (parentScope != null && parentScope != elementContainer) {
//            assert(false); // FIXME
            recurseFindOverrides(parentScope)
        }
    }

    /**
     * Given a module definition, try to get parent scope
     */
    private fun tryToGetParentScope(element: PsiElement) =
            PsiTreeUtil.findFirstParent(element, true) { it is ORBlock } as? ORBlock

    fun findOverrides() =
            tryToGetParentScope(element)?.let { recurseFindOverrides(it) }
}
