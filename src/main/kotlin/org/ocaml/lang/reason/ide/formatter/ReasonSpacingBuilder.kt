package org.ocaml.lang.reason.ide.formatter

import com.intellij.formatting.SpacingBuilder
import com.intellij.psi.codeStyle.CodeStyleSettings
import org.ocaml.lang.reason.ReasonLanguage
import org.ocaml.lang.reason.psi.ReasonTypes


/**
 * SpacingBuilder which controls how to space things
 */
class ReasonSpacingBuilder(settings: CodeStyleSettings) : SpacingBuilder(settings, ReasonLanguage) {
    init {
        this
                .around(ReasonTypes.EQ)
                .spaceIf(settings.getCommonSettings(ReasonLanguage.id).SPACE_AROUND_ASSIGNMENT_OPERATORS)
                .after(ReasonTypes.TYPE_NAME)
                .none()
    }
}
