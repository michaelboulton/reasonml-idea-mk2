package org.ocaml.lang.reason.ide.annotators

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import org.ocaml.lang.reason.psi.*
import org.ocaml.lang.shared.ide.SharedColours

object ReasonExtraColourAnnotator : Annotator {
    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        val annotateAt = getExtraColourFor(element) ?: return

        holder.createInfoAnnotation(annotateAt.first, null).textAttributes = annotateAt.second.textAttributesKey
    }

}

// TODO: Extra colours for arrows depending on whether it's a function definition or a switch/whatever
private fun getExtraColourFor(element: PsiElement): Pair<TextRange, SharedColours>? {
    return when (element) {
        is ReasonModuleName -> element.capitalidentifier.textRange to SharedColours.MODULE_NAME
        is ReasonVariantName -> element.capitalidentifier.textRange to SharedColours.VARIANT_NAME
        is ReasonModuleTypeName -> element.capitalidentifier.textRange to SharedColours.MODULE_TYPE_NAME
        is ReasonTypeName -> element.identifier.textRange to SharedColours.TYPE_NAME
        is ReasonInfixOperator, is ReasonSymbolicInfixOperator -> element.textRange to SharedColours.OPERATOR
        else -> null
    }
}
