package org.ocaml.lang.shared.ide

import com.intellij.navigation.ChooseByNameContributor
import com.intellij.navigation.NavigationItem
import com.intellij.openapi.project.Project
import org.ocaml.lang.reason.psi.ReasonPsiUtil

object ORChooseByNameContributor : ChooseByNameContributor {
    override fun getNames(project: Project, includeNonProjectItems: Boolean): Array<String> {
        val properties = ReasonPsiUtil.findTypeDefinitions(project)
        return properties
                .mapNotNull { it.name }
                .filter { it.isNotBlank() }
                .toTypedArray()
    }

    override fun getItemsByName(name: String, pattern: String, project: Project, includeNonProjectItems: Boolean): Array<NavigationItem> {
        // todo include non project items
        val properties = ReasonPsiUtil.findTypeDefinitionsByName(project, name)
        return properties.map { it as NavigationItem }.toTypedArray()
    }
}
