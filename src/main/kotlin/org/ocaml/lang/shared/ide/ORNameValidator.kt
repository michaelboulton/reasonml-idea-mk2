package org.ocaml.lang.shared.ide

import com.intellij.lang.refactoring.NamesValidator
import com.intellij.lexer.FlexAdapter
import com.intellij.openapi.project.Project
import com.intellij.psi.tree.IElementType
import org.ocaml.lang.reason.psi.REASON_KEYWORDS
import org.ocaml.lang.reason.psi.REASON_KEYWORDS_EXTRA
import org.ocaml.lang.reason.psi.ReasonTypes
import org.ocaml.lang.shared.lexer._ORLexer
import org.ocaml.lang.shared.psi.ReasonTypeBridge


/**
 * Validates names for ocaml and reason
 *
 * They have the same concept of a 'valid' name, so this just uses Reason types
 */
object ORNameValidator : NamesValidator {

    override fun isKeyword(name: String, project: Project?): Boolean {
        val lexedType = getLexerType(name)
        return lexedType in (REASON_KEYWORDS) ||
                lexedType in (REASON_KEYWORDS_EXTRA)
    }

    override fun isIdentifier(name: String, project: Project?): Boolean {
        return when (getLexerType(name)) {
            ReasonTypes.IDENTIFIER, ReasonTypes.CAPITALIDENTIFIER -> true
            else -> false
        }
    }

    private fun getLexerType(text: String): IElementType? {
        val lexer = FlexAdapter(_ORLexer(ReasonTypeBridge))
        lexer.start(text)
        return if (lexer.tokenEnd == text.length) lexer.tokenType else null
    }
}
