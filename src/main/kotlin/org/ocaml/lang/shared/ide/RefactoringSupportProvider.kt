package org.ocaml.lang.shared.ide


import com.intellij.lang.refactoring.RefactoringSupportProvider
import com.intellij.psi.PsiElement
import org.ocaml.lang.shared.psi.ORModuleName
import org.ocaml.lang.shared.psi.ORTypeName

object RefactoringSupportProvider : RefactoringSupportProvider() {
    override fun isMemberInplaceRenameAvailable(element: PsiElement, context: PsiElement?): Boolean {
        return element is ORTypeName || element is ORModuleName
    }
}
