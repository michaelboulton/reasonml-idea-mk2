package org.ocaml.lang.shared.ide

import com.intellij.ide.projectView.PresentationData
import com.intellij.ide.structureView.*
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement
import com.intellij.ide.util.treeView.smartTree.Sorter
import com.intellij.ide.util.treeView.smartTree.TreeElement
import com.intellij.lang.PsiStructureViewFactory
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.editor.Editor
import com.intellij.psi.NavigatablePsiElement
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import org.ocaml.lang.ocaml.psi.OCamlPsiFile
import org.ocaml.lang.reason.psi.*
import org.ocaml.lang.shared.psi.ORModuleContentsContainer
import org.ocaml.lang.shared.psi.ORModuleSignatureContentsContainer
import org.ocaml.lang.shared.psi.ext.ORBlock
import org.ocaml.lang.shared.psi.ext.ORNavigationElement
import org.ocaml.lang.shared.psi.ext.isModuleBlock


/**
 * Plugin entry point
 */
object ORStructureViewFactory : PsiStructureViewFactory {
    override fun getStructureViewBuilder(psiFile: PsiFile): StructureViewBuilder? {
        return object : TreeBasedStructureViewBuilder() {
            override fun createStructureViewModel(editor: Editor?): StructureViewModel {
                return ORStructureViewModel(psiFile)
            }
        }
    }
}


/**
 * Controls how items are shown in the structure view
 */
class ORStructureViewModel(psiFile: PsiFile) : StructureViewModelBase(psiFile, ORStructureViewElement(psiFile)), StructureViewModel.ElementInfoProvider {
    override fun getSorters(): Array<Sorter> = arrayOf(Sorter.ALPHA_SORTER)

    //    TODO: Need to not show the psi file - should present is just as a module
    override fun isAlwaysShowsPlus(element: StructureViewTreeElement): Boolean = element is ORBlock

    override fun isAlwaysLeaf(element: StructureViewTreeElement): Boolean = element is ORNavigationElement && element !is ORBlock
}


/**
 * An item in the structure view
 */
class ORStructureViewElement(private val element: NavigatablePsiElement) : StructureViewTreeElement, SortableTreeElement {
    override fun getValue(): Any = element

    override fun navigate(requestFocus: Boolean) = element.navigate(requestFocus)

    override fun canNavigate(): Boolean = element.canNavigate()

    override fun canNavigateToSource(): Boolean = element.canNavigateToSource()

    override fun getAlphaSortKey(): String = element.name ?: ""

    override fun getPresentation(): ItemPresentation = element.presentation ?: PresentationData()

    override fun getChildren(): Array<TreeElement> {
        when (element) {
            is ReasonPsiFile, is OCamlPsiFile, is ORBlock -> {
//            There should be exactly one of these present
                val element = PsiTreeUtil.getChildOfAnyType(element, ORModuleContentsContainer::class.java, ORModuleSignatureContentsContainer::class.java)!!

                return getStructuralChildrenOfModule(element)
            }

            is ReasonModuleDefinitionStmt -> {
                (element.moduleRhs as? ReasonBracedModuleDefinition)
                        ?.let {
                            return getStructuralChildrenOfModule(it.moduleContentsContainer)
                        }
            }

            is ReasonModuleTypeDefinitionStmt -> {
                (element.moduleTypeDefinition as? ReasonBracedModuleTypeDefinition)
                        ?.let {
                            return getStructuralChildrenOfModule(it.moduleSignatureContentsContainer)
                        }
            }
        }

        return emptyArray()
    }
}


/**
 * Get the children of a block of code to show (should be a module or a module signature)
 */
internal fun getStructuralChildrenOfModule(element: ORBlock): Array<TreeElement> {
//            Let definitions, but only at the module level, where they could actually be imported/used
    val letDefs = if (element.showLetStatementsFor()) {
        findChildrenOf<ReasonLetStmt, ReasonLetAssignment>(element)
//                        Doing "let () = call_a_func()" should not show up in the structure
                .filter {
                    (it.letAssignedTo as? ReasonConstantPattern)?.unit != null
                            ||
                            (it.letAssignedTo as? ReasonUnderscorePattern) != null
                }
    } else {
        emptyList()
    }

//            Type definitions
    val typeDefs = findChildrenOf<ReasonOneOrMoreTypeDefinitionStmt, ReasonTypeDefinition>(element)

//            And any other ones
    val childItems = PsiTreeUtil.getChildrenOfAnyType(element, ORNavigationElement::class.java)
            .plus(typeDefs)
            .plus(letDefs)

    return childItems
            .map { ORStructureViewElement(it) }
            .toTypedArray()
}


/**
 * Whether let statements should be shown in this block in the structure view
 */
internal fun PsiElement.showLetStatementsFor(): Boolean {
    return ((this as? ORBlock)?.isModuleBlock() ?: false)
}


/**
 * Find some definitions which are wrapped in another statement - ie, anything you can chain with an "and"
 */
inline fun <reified P : PsiElement, reified C : ORNavigationElement> findChildrenOf(element: PsiElement): List<C> = (
        PsiTreeUtil.getChildrenOfType(element, P::class.java)
                ?.flatMap {
                    PsiTreeUtil.getChildrenOfType(it, C::class.java)?.toList()
                            ?: emptyList()
                }
                ?: emptyList()
        ).filterNotNull()

