package org.ocaml.lang.shared.ide

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.codeInspection.LocalQuickFix
import com.intellij.codeInspection.ProblemHighlightType
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.psi.PsiElement
import org.ocaml.lang.reason.ide.annotators.inferredType
import org.ocaml.lang.reason.psi.ReasonExpr
import org.ocaml.lang.shared.psi.ORModuleName
import org.ocaml.lang.shared.psi.ORStmt
import org.ocaml.lang.shared.psi.ext.ORBlock
import org.ocaml.lang.shared.psi.ext.blockTypeName


/**
 * Error and warning codes
 *
 * Some things are compilation errors like wrong types, not defined, etc, but some are compiler warnings which can be turned on/off at will
 *
 * TODO: Parse these optional things, or have a setting, or read them from dune, etc.
 */
sealed class ORDiagnostics(val element: PsiElement) {
    abstract fun prepareAnnotation(): PreparedAnnotation

    class ListTypeMismatchError(private val firstElement: ReasonExpr, private val thisElement: ReasonExpr) : ORDiagnostics(thisElement) {
        override fun prepareAnnotation() = PreparedAnnotation(
                Severity.ERROR,
                "This type does not match the first element - was ${thisElement.inferredType}, expected ${firstElement.inferredType}"
        )
    }

    class StatementScopeError(private val statement: ORStmt, private val block: ORBlock) : ORDiagnostics(statement) {
        override fun prepareAnnotation() = PreparedAnnotation(
                Severity.ERROR,
                "Statement of type ${statement::class.java} cannot be used in ${block.blockTypeName}",
                WarningCode.SEMANTICS_ERR
        )
    }

    class OveriddenModuleNameWarning(statement: ORModuleName) : ORDiagnostics(statement) {
        override fun prepareAnnotation() = PreparedAnnotation(
                Severity.WARN,
                "Module name overrides an earlier definition"
        )
    }

    /**
     * TODO: Make this more generic for use with module names as well
     */
    class BadTypeNameError(statement: PsiElement) : ORDiagnostics(statement) {
        override fun prepareAnnotation(): PreparedAnnotation = PreparedAnnotation(
                Severity.ERROR,
                "Only a lowercase name can be used for a type definition"
        )

    }
}


fun ORDiagnostics.addToHolder(holder: AnnotationHolder) {
    val prepared = prepareAnnotation()

    val textRange = element.textRange

    val ann = holder.createAnnotation(
            prepared.severity.toHighlightSeverity(),
            textRange,
            simpleHeader(prepared.errorCode, prepared.header),
            "<html>${htmlHeader(prepared.errorCode, prepared.header)}<br>${prepared.description}</html>"
    )

    ann.highlightType = prepared.severity.toProblemHighlightType()

    prepared.fixes.map { fix ->
        if (fix is IntentionAction) {
            ann.registerFix(fix)
        } else {
            val descriptor = com.intellij.codeInspection.InspectionManager.getInstance(element.project)
                    .createProblemDescriptor(
                            element,
                            element,
                            ann.message,
                            prepared.severity.toProblemHighlightType(),
                            true,
                            fix
                    )

            ann.registerFix(fix, null, null, descriptor)
        }
    }
}


enum class Severity {
    INFO, WARN, ERROR, UNKNOWN_SYMBOL;

    fun toHighlightSeverity(): HighlightSeverity = when (this) {
        INFO -> HighlightSeverity.INFORMATION
        WARN -> HighlightSeverity.WARNING
        ERROR, UNKNOWN_SYMBOL -> HighlightSeverity.ERROR
    }

    fun toProblemHighlightType(): ProblemHighlightType = when (this) {
        INFO -> ProblemHighlightType.INFORMATION
        WARN -> ProblemHighlightType.WEAK_WARNING
        ERROR -> ProblemHighlightType.GENERIC_ERROR_OR_WARNING
        UNKNOWN_SYMBOL -> ProblemHighlightType.LIKE_UNKNOWN_SYMBOL
    }
}

/**
 * TODO:
 * Generic errors should be able to point to a specific part of the ocaml manual?
 *
 * TODO:
 * https://caml.inria.fr/pub/docs/manual-ocaml/comp.html#sec288
 */
enum class WarningCode {
    GENERIC_ERR,
    SEMANTICS_ERR,
    W1, A;

    val code: String
        get() = toString()
}

/**
 * @property errorCode Will only be present if this is a compiler warning
 */
data class PreparedAnnotation(
        val severity: Severity,
        val header: String,
        val errorCode: WarningCode = WarningCode.GENERIC_ERR,
        val description: String = "",
        val fixes: List<LocalQuickFix> = emptyList()
)

private fun simpleHeader(error: WarningCode, description: String): String =
        "$description [${error.code}]"

private fun htmlHeader(error: WarningCode, description: String): String =
        "$description [<a href='https://caml.inria.fr/pub/docs/manual-ocaml/comp.html#sec288'>${error.code}</a>]"
