package org.ocaml.lang.shared.ide

import com.intellij.lang.cacheBuilder.DefaultWordsScanner
import com.intellij.lang.cacheBuilder.WordsScanner
import com.intellij.lang.findUsages.FindUsagesProvider
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNamedElement
import com.intellij.psi.tree.TokenSet
import org.ocaml.lang.reason.parser.ReasonLexer
import org.ocaml.lang.reason.psi.ReasonTypes
import org.ocaml.lang.shared.psi.ORModuleDefinitionStmt
import org.ocaml.lang.shared.psi.ORTypeDefinition
import org.ocaml.lang.shared.psi.ORTypeName
import org.ocaml.lang.shared.psi.ext.ORNamedElement


object ORFindUsagesProvider : FindUsagesProvider {
    override fun getWordsScanner(): WordsScanner? {
        return DefaultWordsScanner(
                ReasonLexer,
//                Identifiers
                TokenSet.create(ReasonTypes.TYPE_NAME, ReasonTypes.MODULE_NAME, ReasonTypes.MODULE_TYPE_NAME),
//                Comments
                TokenSet.create(ReasonTypes.COMMENT),
//                Literals
                TokenSet.create(ReasonTypes.STRING)
        )
    }

    override fun canFindUsagesFor(psiElement: PsiElement): Boolean = psiElement is PsiNamedElement

    override fun getHelpId(psiElement: PsiElement): String? = null

    override fun getType(element: PsiElement): String = when (element as? ORNamedElement) {
        is ORTypeName -> "type name"
        is ORTypeDefinition -> "type definition"
        is ORModuleDefinitionStmt -> "module definition"
        else -> ""
    }

    override fun getDescriptiveName(element: PsiElement): String = when (element) {
        is ORTypeName -> "usage of type ${getNodeText(element, false)}"
        is ORTypeDefinition -> "${getNodeText(element, false)} (defined in ${element.containingFile.name})"
        is ORModuleDefinitionStmt -> "${getNodeText(element, false)} (defined in ${element.containingFile.name})"
        else -> null
    } ?: ""
//            (element as? ORTypeName)?.fullyQualifiedTypeName ?: ""

    override fun getNodeText(element: PsiElement, useFullName: Boolean): String = when (element) {
        is ORTypeName -> element.identifier.text
        is ORTypeDefinition -> element.name
        is ORModuleDefinitionStmt -> element.name
        else -> null
    } ?: ""
//            (element as? ORTypeName)?.fullyQualifiedTypeName ?: ""
}
