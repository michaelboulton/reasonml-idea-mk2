package org.ocaml.lang.shared.ide

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.options.colors.AttributesDescriptor

/**
 * Similar to the RsColor
 */
enum class SharedColours(colourPageName: String, val inheritedAttribute: TextAttributesKey? = null) {
    OPERATOR("Operator", DefaultLanguageHighlighterColors.OPERATION_SIGN)
    ,
    COMMENT("Comment", DefaultLanguageHighlighterColors.LINE_COMMENT)
    ,
    DOC_BLOCK("Documentation block", DefaultLanguageHighlighterColors.DOC_COMMENT)
    ,
    JSX("JSX tags", DefaultLanguageHighlighterColors.MARKUP_TAG)
    ,
    KEYWORD("Keyword", DefaultLanguageHighlighterColors.KEYWORD)
    ,
    KEYWORD_EXTRA("Extra Keywords", DefaultLanguageHighlighterColors.LABEL)
    ,
    IDENTIFIER("Identifier", DefaultLanguageHighlighterColors.IDENTIFIER)
    ,
    STRING("String", DefaultLanguageHighlighterColors.STRING)

    ,
    PAREN("Parentheses", DefaultLanguageHighlighterColors.PARENTHESES)
    ,
    BRACE("Braces", DefaultLanguageHighlighterColors.BRACES)
    ,
    ARRAY("Array Brackets", DefaultLanguageHighlighterColors.BRACKETS)
    ,
    BRACKET("Brackets", DefaultLanguageHighlighterColors.BRACKETS)

//    This can be done at any point - handled by the lexer
//    ,CAPITALIDENTIFIER("Capitalized Identifier", DefaultLanguageHighlighterColors.CLASS_NAME)
//    These ones depend on _where_ the thing is defined - it can be a module type, or a variant, or a polymorphic variant, etc etc etc. This has to be done _separately_
    ,
    MODULE_NAME("Module Name", DefaultLanguageHighlighterColors.CLASS_NAME)
    ,
    MODULE_TYPE_NAME("Module Type Name (best effort)", DefaultLanguageHighlighterColors.INTERFACE_NAME)
    ,
    VARIANT_NAME("Variant Name", DefaultLanguageHighlighterColors.CLASS_NAME)

    ,
    TYPE_NAME("Type Name (best effort)", DefaultLanguageHighlighterColors.INTERFACE_NAME)
    ;

    val textAttributesKey = TextAttributesKey.createTextAttributesKey("org.ocaml.lang.shared.$name", inheritedAttribute)
    val attributesDescriptor = AttributesDescriptor(colourPageName, textAttributesKey)
    val testSeverity: HighlightSeverity = HighlightSeverity(name, HighlightSeverity.INFORMATION.myVal)
}
