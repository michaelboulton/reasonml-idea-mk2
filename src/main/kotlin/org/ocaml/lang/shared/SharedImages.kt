package org.ocaml.lang.shared

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon


object SharedImages {
    val opamIcon: Icon = IconLoader.getIcon("/img/icons/opam_16x16.svg")
    val bucklescriptIcon = IconLoader.getIcon("/img/icons/bucklescript_16x16.svg")
}
