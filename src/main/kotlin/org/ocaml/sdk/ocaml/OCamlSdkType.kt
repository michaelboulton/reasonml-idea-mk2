package org.ocaml.sdk.ocaml

import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.projectRoots.*
import org.jdom.Element
import org.ocaml.lang.shared.SharedImages
import org.ocaml.sdk.ocaml.types.EsySdkType
import org.ocaml.sdk.ocaml.types.OCamlSdkType
import org.ocaml.sdk.ocaml.types.OpamSdkType
import org.ocaml.sdk.ocaml.types.PackageManagerSdkType
import javax.swing.Icon

internal val log = Logger.getInstance("org.ocaml.sdk.ocaml")

object OCamlSdkType : SdkType("OCaml") {
    private val sdkHomeTypeCache: MutableMap<String, OCamlSdkType> = mutableMapOf()

    override fun getPresentableName(): String = "OCaml"

    override fun getIcon(): Icon = SharedImages.opamIcon

    override fun getVersionString(sdkHome: String?): String? =
            sdkHome?.let { sdkHomeTypeCache[sdkHome] }?.getVersionString(sdkHome)

    override fun isValidSdkHome(path: String): Boolean =
            sdkTypes.filter {
                val isValid = it.isValidSdkHome(path)
                log.debug("Is $path a valid for ${it.sdkIdent}: $isValid")
                if (isValid) {
                    sdkHomeTypeCache[path] = it
                }
                isValid
            }.any()

    override fun suggestSdkName(currentSdkName: String?, sdkHome: String): String {
        val sdkType = sdkHomeTypeCache[sdkHome]

        sdkType ?: return ""

        return sdkType.getVersionString(sdkHome)?.let {
            "${sdkType.sdkIdent} $it"
        } ?: sdkType.sdkIdent
    }

    override fun suggestHomePath(): String? = suggestHomePaths().firstOrNull()

    override fun suggestHomePaths(): MutableCollection<String> =
            sdkTypes.map {
                val paths = it.suggestHomePaths()

                log.debug("Suggested paths for ${it.sdkIdent}: $paths")

                paths.map { home ->
                    sdkHomeTypeCache[home] = it
                }

                paths
            }.toList().flatten().toMutableSet()

    /**
     * Nothing to configure
     */
    override fun createAdditionalDataConfigurable(sdkModel: SdkModel, sdkModificator: SdkModificator): AdditionalDataConfigurable? = null

    /**
     * Blank on purpose
     */
    override fun saveAdditionalData(additionalData: SdkAdditionalData, additional: Element) {}

    private val sdkTypes = arrayOf(
            EsySdkType,
            OpamSdkType,
            PackageManagerSdkType
    )
}
