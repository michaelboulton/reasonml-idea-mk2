package org.ocaml.sdk.ocaml

import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Call 'ocaml -version' and regex out the version
 */
internal fun getOCamlVersionFromExecutable(sdkHome: String): String? {
    val executable = File(File(sdkHome, "bin"), "ocaml")

    if (!executable.isFile) return null

    val proc = ProcessBuilder(executable.absolutePath, "-version")
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
    proc.waitFor(5, TimeUnit.SECONDS)

    val output = proc.inputStream.bufferedReader().readText()

    val regex = Regex(".*OCaml.*version (.+)")
    val match = regex.find(output)

    return if (match != null) {
        log.info("SDK version ${match.groups}")
        match.groups[1]!!.value
    } else {
        log.info("Unable to get sdk version")
        null
    }
}

/**
 * Figure out if this is an ocaml sdk path by detecting if all the relevant files are there
 */
internal fun isOcamlHomePath(dir: File): Boolean {
    val binDir = File(dir, "bin")
    if (!binDir.isDirectory) return false

    val binFiles = binDir.listFiles()

    binFiles ?: return false

    return binFiles.map { it.name }.containsAll(ocamlSdkFiles)
}

/**
 * What is considered part of an sdk
 */
private val ocamlSdkFiles = setOf(
        "ocamlopt",
        "ocamlc",
        "ocaml"
)

