package org.ocaml.sdk.ocaml.types

import org.ocaml.lang.shared.SharedImages
import org.ocaml.sdk.ocaml.isOcamlHomePath
import java.io.File
import javax.swing.Icon

abstract class OCamlSdkType {
    abstract fun getVersionString(sdkHome: String): String?
    abstract fun suggestHomePaths(): MutableCollection<String>

    abstract val sdkIdent: String

    open fun getIcon(): Icon = SharedImages.opamIcon
    open fun isValidSdkHome(path: String): Boolean = isOcamlHomePath(File(path))
}
