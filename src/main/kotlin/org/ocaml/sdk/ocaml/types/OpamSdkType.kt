package org.ocaml.sdk.ocaml.types

import org.ocaml.sdk.getSdkPaths
import org.ocaml.sdk.ocaml.isOcamlHomePath
import java.io.File

/**
 * Folders that opam creates
 */
private val opamFolders = setOf(
        "download-cache",
        "log",
        "opam-init",
        "repo"
)

fun getOpamSdkFolders(): Array<out String> {
    val opamHome = File(System.getProperty("user.home"), ".opam")
    val possibleFolders = opamHome
            .listFiles { file -> (!opamFolders.contains(file.name)) && file.isDirectory }

    return getSdkPaths(possibleFolders, ::isOcamlHomePath)
}

object OpamSdkType : OCamlSdkType() {
    override val sdkIdent: String
        get() = "Opam"

    override fun getVersionString(sdkHome: String): String? = File(sdkHome).nameWithoutExtension

    override fun suggestHomePaths(): MutableCollection<String> = getOpamSdkFolders().toMutableSet()
}
