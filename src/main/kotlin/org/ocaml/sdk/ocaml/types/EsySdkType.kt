package org.ocaml.sdk.ocaml.types

import org.ocaml.sdk.getSdkPaths
import org.ocaml.sdk.ocaml.isOcamlHomePath
import java.io.File


fun getEsySdkFolders(): Array<String> {
    val esyHome = File(System.getProperty("user.home"), "3__________________________________________________________________/i")

    val possibleFolders = esyHome
            .listFiles { file -> file.isDirectory && file.name.startsWith("ocaml") }

    return getSdkPaths(possibleFolders, ::isOcamlHomePath)
}

object EsySdkType : OCamlSdkType() {
    override val sdkIdent: String
        get() = "Esy"

    override fun getVersionString(sdkHome: String): String? = File(sdkHome).nameWithoutExtension

    override fun suggestHomePaths(): MutableCollection<String> = getEsySdkFolders().toMutableSet()
}
