package org.ocaml.sdk.ocaml.types

import org.ocaml.sdk.getSdkPaths
import org.ocaml.sdk.ocaml.getOCamlVersionFromExecutable
import org.ocaml.sdk.ocaml.isOcamlHomePath
import java.io.File


private val packageManagerLocations = setOf(
        "/usr/",
        "/usr/local"
)


fun getBuiltinSdkFolders(): Array<out String> {
    return getSdkPaths(packageManagerLocations.map { File(it) }.toTypedArray(), ::isOcamlHomePath)
}


object PackageManagerSdkType : OCamlSdkType() {
    override val sdkIdent: String
        get() = "Managed"

    /**
     * builtin: 4.07
     */
    override fun getVersionString(sdkHome: String): String? = getOCamlVersionFromExecutable(sdkHome)

    override fun suggestHomePaths(): MutableCollection<String> = getBuiltinSdkFolders().toMutableSet()
}
