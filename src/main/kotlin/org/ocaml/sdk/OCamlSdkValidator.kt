package org.ocaml.sdk

import com.intellij.codeInsight.daemon.ProjectSdkSetupValidator
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.module.ModuleUtilCore
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectBundle
import com.intellij.openapi.roots.ModuleRootManager
import com.intellij.openapi.roots.ModuleRootModificationUtil
import com.intellij.openapi.roots.ui.configuration.ProjectSettingsService
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiManager
import org.ocaml.lang.ocaml.OCamlFileType
import org.ocaml.lang.ocaml.OCamlLanguage
import org.ocaml.lang.reason.ReasonFileType
import org.ocaml.lang.reason.ReasonLanguage


/**
 * Mostly copied from [com.intellij.codeInsight.daemon.impl.JavaProjectSdkSetupValidator]
 *
 * This is called OCamlSdkValidator, but it could result in a bucklescript SDK being used
 *
 * TODO: newer versions of intellij have 'SdkPopupFactory' - use that instead
 */
object OCamlSdkValidator : ProjectSdkSetupValidator {
    /**
     * Detects whether this validator is applicable to this file/project
     */
    override fun isApplicableFor(project: Project, file: VirtualFile): Boolean =
            file.fileType in arrayOf(OCamlFileType, ReasonFileType)
                    &&
                    PsiManager.getInstance(project).findFile(file)?.let {
                        it.language in arrayOf(OCamlLanguage, ReasonLanguage)
                    } ?: false

    /**
     * prompt(?) the user and apply sdk to the project/module
     */
    override fun doFix(project: Project, file: VirtualFile) {
        ProjectSettingsService.getInstance(project).chooseAndSetSdk()
                ?.let { ModuleUtilCore.findModuleForFile(file, project) }
                ?.let { WriteAction.run<RuntimeException> { ModuleRootModificationUtil.setSdkInherited(it) } }
    }

    /**
     * Error message shown to user when there is no sdk defined for file in module?
     */
    override fun getErrorMessage(project: Project, file: VirtualFile): String? =
            ModuleUtilCore.findModuleForFile(file, project)?.let { module ->
                if (module.isDisposed) return null

                ModuleRootManager.getInstance(module).sdk
                        ?.let {
                            if (ModuleRootManager.getInstance(module).isSdkInherited) {
                                ProjectBundle.message("project.sdk.not.defined")
                            } else {
                                ProjectBundle.message("module.sdk.not.defined")
                            }
                        }
            }
}
