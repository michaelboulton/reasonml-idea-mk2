package org.ocaml.sdk.bucklescript

import com.intellij.openapi.module.ModuleManager
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.projectRoots.*
import org.jdom.Element
import org.ocaml.lang.shared.SharedImages
import org.ocaml.sdk.getSdkPaths
import org.ocaml.sdk.pathFrom
import java.io.File
import java.util.concurrent.TimeUnit
import javax.swing.Icon


val bucklescriptLocation = setOf(
        "node_modules/.bin/bsb"
)

val bucklescriptSdkFiles = listOf("bsc", "bsb")


fun isBucklescriptNodeModulesHomePath(dir: File): Boolean {
    val bsPlatformDir = pathFrom(dir.absolutePath, "node_modules", ".bin")
    if (!bsPlatformDir.isDirectory) return false

    return isBucklescriptHomePath(bsPlatformDir)
}

/**
 * Whether bsb and bsc exist
 * @param binDir node_modules/.bin folder in the sdk
 */
fun isBucklescriptHomePath(binDir: File): Boolean {
    val binFiles = binDir.listFiles()

    binFiles ?: return false

    return binFiles.map { it.name }.containsAll(bucklescriptSdkFiles)
}


fun getBucklescriptSdkFolders(modulePath: String): Array<out String> {
    val folders = bucklescriptLocation
            .map {
                File(modulePath, it)
            }.toTypedArray()

    return getSdkPaths(folders, ::isBucklescriptHomePath)
}


/**
 * Call 'ocaml -version' and regex out the version
 */
internal fun getBucklescriptVersionFromExecutable(sdkHome: String): String? {
    val executable = pathFrom(sdkHome, "node_modules", ".bin", "bsb")

    if (!executable.isFile) return null

    val proc = ProcessBuilder(executable.absolutePath, "-version")
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
    proc.waitFor(5, TimeUnit.SECONDS)

    return proc.inputStream.bufferedReader().readText()
}


object BucklescriptSdkType : SdkType("Bucklescript") {
    override fun getPresentableName(): String = "Bucklescript"

    override fun getIcon(): Icon = SharedImages.bucklescriptIcon

    override fun getVersionString(sdkHome: String?): String? {
        sdkHome?.let {
            val naive = getBucklescriptVersionFromExecutable(sdkHome)
            if (naive != null) return naive
        }
        return "unknown"
    }

    override fun isValidSdkHome(path: String): Boolean {
        val dir = File(path)
        return isBucklescriptNodeModulesHomePath(dir)
    }

    override fun suggestSdkName(currentSdkName: String?, sdkHome: String): String {
        return getVersionString(sdkHome)
                ?.let {
                    "Bucklescript $it"
                } ?: "Bucklescript"
    }

    override fun suggestHomePath(): String? = suggestHomePaths().firstOrNull()

    override fun suggestHomePaths(): MutableCollection<String> {
        val zilch = emptySet<String>().toMutableSet()

        val project = ProjectManager.getInstance()
                ?.openProjects
                ?.get(0)

        project ?: return zilch

        val modules = ModuleManager.getInstance(project).modules

        return modules
                .map { getBucklescriptSdkFolders(it.moduleFilePath).toList() }
                .flatten().toMutableSet()
    }

    /**
     * Nothing to configure
     */
    override fun createAdditionalDataConfigurable(sdkModel: SdkModel, sdkModificator: SdkModificator): AdditionalDataConfigurable? = null

    /**
     * Blank on purpose
     */
    override fun saveAdditionalData(additionalData: SdkAdditionalData, additional: Element) {}
}
