package org.ocaml.sdk

import java.io.File

/**
 * Given list of path fragments, create a new file from it
 */
fun pathFrom(vararg fragments: String): File = File(fragments.joinToString(File.separator))

/**
 * Given a list of possible sdk folders, filter out the ones which are valid and return the base file name
 */
internal fun getSdkPaths(folders: Array<File>?, validator: (File) -> Boolean): Array<String> {
    folders ?: return emptyArray()

    return folders
            .filter { validator(it) }
            .map { it.name }
            .toTypedArray()
}
