package org.ocaml

import com.github.javaparser.StaticJavaParser
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.nodeTypes.NodeWithExtends
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*


val logger = LoggerFactory.getLogger("JavaUtils")


val NodeWithExtends<*>.isPsiElement: Boolean
    get() = extendedTypes.find { it.nameAsString.contains("PsiElement") } != null


val ClassOrInterfaceDeclaration.isReasonInterface: Boolean
    get() = nameAsString.contains("Reason")


internal fun getReasonInterface(parsedFile: CompilationUnit): Optional<ClassOrInterfaceDeclaration> {
    val firstInterfaceDeclaration = parsedFile.findFirst(ClassOrInterfaceDeclaration::class.java)
    return firstInterfaceDeclaration.filter {
        logger.debug(it.nameAsString)
        logger.debug(" interface: ${it.isInterface}")
        logger.debug(" reason: ${it.isReasonInterface}")
//        org.ocaml.getLogger.info(" psi element: ${it.org.ocaml.isPsiElement}")
        it.isInterface && it.isReasonInterface //&& it.org.ocaml.isPsiElement
    }
}


internal fun getOnlyInterfaces(interfaceFile: File): Boolean {
    val parsed = StaticJavaParser.parse(interfaceFile)

    val psiInterface = getReasonInterface(parsed)

    return psiInterface.isPresent
}
