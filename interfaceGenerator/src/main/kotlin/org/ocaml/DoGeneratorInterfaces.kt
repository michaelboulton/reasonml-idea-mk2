package org.ocaml

import com.github.javaparser.StaticJavaParser
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.ImportDeclaration
import com.github.javaparser.ast.NodeList
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.expr.SimpleName
import com.github.javaparser.ast.type.ClassOrInterfaceType
import org.gradle.api.GradleException
import java.io.File

internal fun doGeneratorInterfaces(options: InterfaceGenPluginOptions, inputFiles: List<File>) {
    val outputAbsolute = File(options.outputFolder)

    if (!outputAbsolute.exists()) {
        outputAbsolute.mkdir()
    }

    inputFiles.map {
        val parsed = StaticJavaParser.parse(it)

        val iface = getReasonInterface(parsed).get()
        fixInterfaceName(iface)

        fixImports(parsed, iface)

        fixInheritance(iface)

//        TODO: Remove or modify members as required

        fixPackageDeclaration(parsed)

        val newFile = File(outputAbsolute, it.name.replace("Reason", "OR", ignoreCase = false))

        newFile.parentFile.mkdirs()

        newFile.writeText(parsed.toString().replace("Reason", "OR"))
    }
}

/**
 * Make package declaration go into the right folder
 */
internal fun fixPackageDeclaration(parsed: CompilationUnit) {
    parsed.setPackageDeclaration("org.ocaml.lang.shared.psi")
}

/**
 * Fix inheritance if it now inherits from itself
 */
internal fun fixInheritance(iface: ClassOrInterfaceDeclaration) {
    val extendedTypes = iface.extendedTypes
            .filter { inherits -> inherits.nameAsString != iface.nameAsString }

    iface.extendedTypes = NodeList(extendedTypes)

    if (extendedTypes.isEmpty() || null == extendedTypes.find { it.nameAsString == "ORElement" }) {
        val fakeInterface = ClassOrInterfaceType()
        fakeInterface.name = SimpleName("ORElement")

        iface.extendedTypes.add(fakeInterface)
    }

    logger.debug("{}:{}", iface.nameAsString, iface.extendedTypes)
}

/**
 * Change imports to import from OR instead of Reason
 *
 * TODO: Change other imports as well?
 */
internal fun fixImports(parsed: CompilationUnit, iface: ClassOrInterfaceDeclaration) {
    //        Remove imports as required
    val correctedImports = parsed.imports
            .map { imp -> ImportDeclaration(imp.nameAsString.replace("Reason", "OR"), imp.isStatic, imp.isAsterisk) }
            .filter { imp -> !imp.nameAsString.contains(iface.nameAsString) }
    parsed.imports = NodeList(correctedImports)

//    And add the import for base element, just in case
    parsed.imports.add(ImportDeclaration("org.ocaml.lang.shared.psi.ORElement", false, false))
}

/**
 * Change ReasonIface to ORIface
 */
internal fun fixInterfaceName(iface: ClassOrInterfaceDeclaration) {
    iface.setName(iface.nameAsString.replace("Reason", "OR"))
}
