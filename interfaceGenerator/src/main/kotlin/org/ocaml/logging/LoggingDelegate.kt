package org.ocaml.logging

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.companionObject

fun <T : Any> getClassForLogging(javaClass: Class<T>): Class<*> {
    return javaClass.enclosingClass?.takeIf {
        it.kotlin.companionObject?.java == javaClass
    } ?: javaClass
}

class LoggingDelegate<in R : Any> : ReadOnlyProperty<R, Logger> {
    private lateinit var logger: Logger

    override fun getValue(thisRef: R, property: KProperty<*>): Logger {
        if (!(::logger.isInitialized)) {
            logger = LoggerFactory.getLogger(getClassForLogging(thisRef.javaClass))
        }

        return logger
    }
}
