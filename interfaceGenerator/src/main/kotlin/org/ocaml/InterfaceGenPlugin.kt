package org.ocaml

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.compile.JavaCompile
import java.io.File


open class InterfaceGenPluginOptions(
        var outputFolder: String = "outputs",
        var inputFolder: String = "inputs"
)

class InterfaceGenPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val options = project.extensions.create("generatorOptions", InterfaceGenPluginOptions::class.java)

        val generateTask = project.tasks.register("generateORInterfaces")

//         WHY?? this should happen automatically!!
        project.afterEvaluate {
            generateTask.configure {
                val inputAbsolute = File(options.inputFolder)

                logger.info("Reading from ${inputAbsolute.absolutePath}")

                val inputFiles = inputAbsolute
                        .walkTopDown()
                        .filter { it.isFile }
                        .filter { it.name.contains("Reason") }
                        .filter { !it.name.contains("ReasonTypes") }
                        .filter { !it.name.contains("Jsx") }
                        .filter { getOnlyInterfaces(it) }
                        .toList()

                it.inputs.files(inputFiles)

                doGeneratorInterfaces(options, it.inputs.files.toList())
            }

        }

        project.tasks.withType(JavaCompile::class.java) {
            it.dependsOn(generateTask)
        }
    }
}
