package org.ocaml

import com.github.javaparser.StaticJavaParser
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.type.ClassOrInterfaceType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class DoGeneratorInterfacesKtTest {

    private val originalText = """
        package org.ocaml.lang.reason.psi;

        import java.util.List;
        import org.jetbrains.annotations.*;
        import com.intellij.psi.PsiElement;
        import org.ocaml.lang.shared.psi.ORStmt;

        public interface ReasonStmt extends ORStmt {

        }
    """.trimIndent()

    @Test
    fun `test fixPackageDeclaration`() {
        val parsed = StaticJavaParser.parse(originalText)

        assertThat(parsed.toString()).doesNotStartWith("package org.ocaml.lang.shared.psi")

        fixPackageDeclaration(parsed)

        assertThat(parsed.toString()).startsWith("package org.ocaml.lang.shared.psi")
    }

    @Test
    fun `test fixInterfaceName`() {
        val parsed = StaticJavaParser.parse(originalText)
        val iface = getReasonInterface(parsed).get()

        assertThat(parsed.toString()).doesNotContain("public interface ORStmt extends")

        fixInterfaceName(iface)

        assertThat(parsed.toString()).contains("public interface ORStmt extends")
    }

    @Nested
    @DisplayName("Test inheritance is not duplicated")
    inner class TestInheritance {
        private val iface: ClassOrInterfaceDeclaration

        init {
            val parsed = StaticJavaParser.parse(originalText)
            iface = getReasonInterface(parsed).get()
            iface.setName("ORStmt")

            fixInheritance(iface)
        }

        @Test
        fun `test only inherits from one thing`() {
            assertThat(iface.extendedTypes).hasSize(1)
        }

        @Test
        fun `test still inherits from orelement`() {
            assertThat(iface.extendedTypes).contains(ClassOrInterfaceType("ORElement"))
        }
    }

    @Nested
    @DisplayName("Test imports are changed correctly")
    inner class TestFixImports {
        private val iface: ClassOrInterfaceDeclaration
        private val importName: List<String>

        init {
            val parsed = StaticJavaParser.parse(originalText)
            iface = getReasonInterface(parsed).get()
            iface.setName("ORStmt")

            fixImports(parsed, iface)

            importName = parsed.imports.map { it.nameAsString }
        }

        @Test
        fun `test ORElement is added to imports`() {
            assertThat(importName).contains("org.ocaml.lang.shared.psi.ORElement")
        }

        @Test
        fun `test ORStmt is not in import any more`() {
            assertThat(importName).doesNotContain("org.ocaml.lang.shared.psi.ORStmt")
        }
    }
}

class TestFixInheritance {
    private val iface: ClassOrInterfaceDeclaration

    init {
        val originalText = """
            package org.ocaml.lang.reason.psi;

            import java.util.List;
            import org.jetbrains.annotations.*;
            import com.intellij.psi.PsiElement;
            import org.ocaml.lang.shared.psi.ORStmt;

            public interface ReasonBloop extends ORStmt, OtherThing {

            }
        """.trimIndent()

        val parsed = StaticJavaParser.parse(originalText)
        iface = getReasonInterface(parsed).get()

        fixInheritance(iface)
    }

    @Test
    fun `test Has 3 inherits`() {
        assertThat(iface.extendedTypes).hasSize(3)
    }

    @Test
    fun `test also inerhtis from orelement`() {
        assertThat(iface.extendedTypes).contains(ClassOrInterfaceType("ORElement"))
    }
}
