package org.ocaml

import com.github.javaparser.StaticJavaParser
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class IsPsiElementTest {

    @Test
    fun `Is psi element`() {
        val javaCode = """
        interface LFlksf extends PsiElement{}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isPsiElement).isTrue()
    }

    @Test
    fun `inherits from multiple, first`() {
        val javaCode = """
        interface LFlksf extends PsiElement, thing1{}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isPsiElement).isTrue()
    }

    @Test
    fun `inherits from multiple, second`() {
        val javaCode = """
        interface LFlksf extends thing1, PsiElement{}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isPsiElement).isTrue()
    }

    @Test
    fun `Inherits from something else`() {
        val javaCode = """
        interface LFlksf extends kodkfo {}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isPsiElement).isFalse()
    }

    @Test
    fun `inherits from nothing`() {
        val javaCode = """
        interface LFlksf {}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isPsiElement).isFalse()
    }

}


internal class IsReasonInterfaceTest {

    @Test
    fun `Is correct interface`() {
        val javaCode = """
        interface ReasonThing {}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isReasonInterface).isTrue()
    }


//    @Test
//    fun `is a class`() {
//        val javaCode = """
//        class ReasonThing {}
//        """.trimIndent()
//
//        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)
//
//        assertThat(firstInterfaceDeclaration.isReasonInterface).isFalse()
//    }

    @Test
    fun `Is an interface with with a bad name`() {
        val javaCode = """
        interface Glkfl {}
        """.trimIndent()

        val firstInterfaceDeclaration = parseAndGetInterface(javaCode)

        assertThat(firstInterfaceDeclaration.isReasonInterface).isFalse()
    }
}

fun parseAndGetInterface(javaCode: String): ClassOrInterfaceDeclaration {
    val parsedFile = StaticJavaParser.parse(javaCode)
    val firstInterfaceDeclaration = parsedFile.findFirst(ClassOrInterfaceDeclaration::class.java).get()
    return firstInterfaceDeclaration
}
