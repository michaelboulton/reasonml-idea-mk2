import org.gradle.api.tasks.testing.logging.TestExceptionFormat


repositories {
    mavenCentral()
    maven("https://dl.bintray.com/jetbrains/markdown")
}

plugins {
    java
    kotlin("jvm") version "1.4.0"
    `java-gradle-plugin`
}


tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_11.toString()
            languageVersion = "1.4"
            apiVersion = "1.4"
        }
    }

    withType<Test> {
        useJUnitPlatform()
        testLogging {
            exceptionFormat = TestExceptionFormat.FULL
        }
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

//    Force tests to pass before building jar
//    NOTE: This is not enabled, because it breaks the way gradle imports the projects
//    jar.configure {
//        dependsOn(test)
//    }

    test {
    }
}

val junit5version = "5.5.2"

dependencies {
    implementation(kotlin("stdlib", "1.4.0"))
    implementation(kotlin("reflect", "1.4.0"))

    implementation("com.github.javaparser:javaparser-core:3.15.7")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junit5version")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit5version")
    testImplementation("org.assertj:assertj-core:3.13.2")
    testImplementation("io.mockk:mockk:1.9.3")
}


gradlePlugin {
    plugins {
        create("commonInterfaceGenPlugin") {
            id = "xyz.boulton.common-interface-generator"
            implementationClass = "org.ocaml.InterfaceGenPlugin"
        }
    }
}
